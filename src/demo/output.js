var DRODemuxer = AV.Demuxer.extend(function() {
    AV.Demuxer.register(this);

    // Aurora methods
    this.prototype.init = function() {
        this.headerData = null;
        this.headerLoaded = false;
    };


    this.probe = function(buffer) {
        return buffer.peekString(0, 8) == 'DBRAWOPL';
    };

    this.prototype.readChunk = function() {
        var stream = this.stream;

        if (!this.headerLoaded) {
            if (stream.offset == 0) {
                if (stream.available(26)) {
                    this.__readHeader();
                } else {
                    return null;
                }
            }
            if (stream.offset == 26) {
                if (stream.available(this.headerData.codemapLength)) {
                    this.emit('format', {
                        formatID: 'dro2',    // <= 4 letter string representing the decoder that should be used
                        sampleRate: 49700,   // the sampleRate of the file
                        channelsPerFrame: 2, // the number of audio channels
                        bitsPerChannel: 16   // the sample bit depth
                    });
                    this.emit('duration', this.headerData.lengthMs);
                    this.__readCodemap();
                } else {
                    return null;
                }
            }
        }

        if (stream.offset >= this.headerData.lengthPairs * 2 + 26 + this.headerData.codemapLength) {
            return this.emit('end');
        }

        while (stream.available(1)) {
            var buffer = stream.readSingleBuffer(stream.remainingBytes());
            this.emit('data', buffer);
        }
    };

    // Internal methods
    this.prototype._readUInt8 = function() {
        return this.stream.readUInt8();
    };

    this.prototype._readUInt16LE = function() {
        return this.stream.readUInt16(true);
    };

    this.prototype._readUInt32LE = function() {
        return this.stream.readUInt32(true);
    };

    this.prototype.__readHeader = function() {
        var headerData = {};
        var header = this.stream.readString(8);
        if (header != 'DBRAWOPL') {
            return this.emit('error', 'Incorrect DRO header, expected DBRAWOPL, found ' + header);
        }
        var ver1 = this._readUInt16LE();
        var ver2 = this._readUInt16LE();
        if (ver1 !== 2 && ver2 !== 0) { // only support DRO v2 for now
            return this.emit('error', 'Unsupported DRO version, expected 2.0, found ' + ver1 + '.' + ver2);
        }
        headerData.droVersion = 2;
        headerData.lengthPairs = this._readUInt32LE();
        headerData.lengthMs = this._readUInt32LE();
        headerData.hardwareType = this._readUInt8();
        var iFormat = this._readUInt8();
        if (iFormat != 0) {
            return this.emit('error', 'Unsupported DRO v2 format, only format 0 is supported; found ' + iFormat);
        }
        var iCompression = this._readUInt8();
        if (iCompression != 0) {
            return this.emit('error', 'Unsupported DRO v2 compression, only compression 0 is supported; found ' + iCompression);
        }
        headerData.shortDelayCode = this._readUInt8();
        headerData.longDelayCode = this._readUInt8();
        headerData.codemapLength = this._readUInt8();
        headerData.codemap = [];
        this.headerData = headerData;
    };

    this.prototype.__readCodemap = function() {
        for (var i = 0; i < this.headerData.codemapLength; i++) {
            this.headerData.codemap[i] = this.stream.readUInt8();
        }
        this.emit('cookie', this.headerData);
        this.headerLoaded = true;
    };
});
var OPL3JSUtil = (function() {
    function OPL3JSUtil() {
    }

    /**
     * Creates an array of the given length, and sets the initial value of each element to "value".
     * @param length
     * @param value
     * @return {Array}
     */
    OPL3JSUtil.initArray = function(length, value) {
        var newArray = [];
        for (var i = 0; i < length; i ++) {
            newArray[i] = value;
        }
        return newArray;
    };

    /**
     * Replaces all values in an array with the given value.
     * @param array
     * @param value
     * @returns void
     */
    OPL3JSUtil.fillArray = function(array, value) {
        for (var i = 0; i < array.length; i ++) {
            array[i] = value;
        }
    };

    /**
     * Required for compatibility with the Java implementation.
     * OPL3 uses "Infinity" values (which are doubles) and casts them to ints.
     * In Javascript, these ints are still "Infinity", but in Java they are 2147483647, the max int value.
     */
    OPL3JSUtil.castToJavaInt = function(in_value) {
        if (in_value == Infinity) {
            return 2147483647;
        }
        return parseInt(in_value);
    };

    // These two methods are taken from CoffeeScript.
    OPL3JSUtil.__hasProp = {}.hasOwnProperty;
    OPL3JSUtil.__extends = function (child, parent) {
        for (var key in parent) {
            if (OPL3JSUtil.__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };

    OPL3JSUtil.OPL3JSException = function(message) {
        this.message = message;
        this.name = 'OPL3JSException';
    };

    /* Read a file using xmlhttprequest

     If the HTML file with your javascript app has been saved to disk,
     this is an easy way to read in a data file. Writing out is
     more complicated and requires either an ActiveX object (IE)
     or XPCOM (Mozilla).

     fname - relative path to the file
     callback - function to call with file text
     */
    OPL3JSUtil.readFileHttp = function(fname, callback) {
        xmlhttp = getXmlHttp();
        xmlhttp.onload = function(e) {
            var uInt8Array = new Uint8Array(this.response);
            return callback(uInt8Array);
        };
        xmlhttp.open("GET", fname, true);
        xmlhttp.responseType = 'arraybuffer';
        xmlhttp.send(null);
    };

    /*
     Return a cross-browser xmlhttp request object
     */
    OPL3JSUtil.getXmlHttp = function() {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if (xmlhttp == null) {
            alert("Your browser does not support XMLHTTP.");
        }
        return xmlhttp;
    };

    return OPL3JSUtil;
})();



var OPL3Data = (function() {
    function OPL3Data() {
    }

    OPL3Data._1_NTS1_6_Offset = 0x08;
    OPL3Data.DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1_Offset = 0xBD;
    OPL3Data._7_NEW1_Offset = 0x105;
    OPL3Data._2_CONNECTIONSEL6_Offset = 0x104;

    OPL3Data.sampleRate = 49700; // not quite right, I think.

    OPL3Data.calculateIncrement = function(begin, end, period) {
        return (end - begin) / OPL3Data.sampleRate * (1 / period);
    };

    OPL3Data.vibratoTable = (function() {
        // According to the YMF262 datasheet, the OPL3 vibrato repetition rate is 6.1 Hz.
        // According to the YMF278B manual, it is 6.0 Hz.
        // The information that the vibrato table has 8 levels standing 1024 samples each
        // was taken from the emulator by Jarek Burczynski and Tatsuyuki Satoh,
        // with a frequency of 6,06689453125 Hz, what  makes sense with the difference
        // in the information on the datasheets.

        // The first array is used when DVB=0 and the second array is used when DVB=1.
        var vibratoTable = [
            OPL3JSUtil.initArray(8192, 0),
            OPL3JSUtil.initArray(8192, 0)
        ];

        var semitone = Math.pow(2, 1 / 12);
        // A cent is 1/100 of a semitone:
        var cent = Math.pow(semitone, 1 / 100);

        // When dvb=0, the depth is 7 cents, when it is 1, the depth is 14 cents.
        var DVB0 = Math.pow(cent, 7);
        var DVB1 = Math.pow(cent, 14);
        var i;
        for (i = 0; i < 1024; i++) {
            vibratoTable[0][i] = vibratoTable[1][i] = 1;
        }
        for (; i < 2048; i++) {
            vibratoTable[0][i] = Math.sqrt(DVB0);
            vibratoTable[1][i] = Math.sqrt(DVB1);
        }
        for (; i < 3072; i++) {
            vibratoTable[0][i] = DVB0;
            vibratoTable[1][i] = DVB1;
        }
        for (; i < 4096; i++) {
            vibratoTable[0][i] = Math.sqrt(DVB0);
            vibratoTable[1][i] = Math.sqrt(DVB1);
        }
        for (; i < 5120; i++) {
            vibratoTable[0][i] = vibratoTable[1][i] = 1;
        }
        for (; i < 6144; i++) {
            vibratoTable[0][i] = 1 / Math.sqrt(DVB0);
            vibratoTable[1][i] = 1 / Math.sqrt(DVB1);
        }
        for (; i < 7168; i++) {
            vibratoTable[0][i] = 1 / DVB0;
            vibratoTable[1][i] = 1 / DVB1;
        }
        for (; i < 8192; i++) {
            vibratoTable[0][i] = 1 / Math.sqrt(DVB0);
            vibratoTable[1][i] = 1 / Math.sqrt(DVB1);
        }
        return vibratoTable;
    })();


    OPL3Data.tremoloTable = (function() {
        var tremoloTable;

        // The OPL3 tremolo repetition rate is 3.7 Hz.
        var tremoloFrequency = 3.7;

        // The tremolo depth is -1 dB when DAM = 0, and -4.8 dB when DAM = 1.
        var tremoloDepth = [-1, -4.8];

        //  According to the YMF278B manual's OPL3 section graph,
        //              the tremolo waveform is not
        //   \      /   a sine wave, but a single triangle waveform.
        //    \    /    Thus, the period to achieve the tremolo depth is T/2, and
        //     \  /     the increment in each T/2 section uses a frequency of 2*f.
        //      \/      Tremolo varies from 0 dB to depth, to 0 dB again, at frequency*2:
        var tremoloIncrement = [
            OPL3Data.calculateIncrement(tremoloDepth[0], 0, 1 / (2 * tremoloFrequency)),
            OPL3Data.calculateIncrement(tremoloDepth[1], 0, 1 / (2 * tremoloFrequency))
        ];

        var tremoloTableLength = (OPL3Data.sampleRate / tremoloFrequency).toFixed();

        // First array used when AM = 0 and second array used when AM = 1.
        tremoloTable = [
            OPL3JSUtil.initArray(tremoloTableLength, 0),
            OPL3JSUtil.initArray(tremoloTableLength, 0)
        ];

        // This is undocumented. The tremolo starts at the maximum attenuation,
        // instead of at 0 dB:
        tremoloTable[0][0] = tremoloDepth[0];
        tremoloTable[1][0] = tremoloDepth[1];
        var counter = 0;
        // The first half of the triangle waveform:
        while (tremoloTable[0][counter] < 0) {
            counter++;
            tremoloTable[0][counter] = tremoloTable[0][counter - 1] + tremoloIncrement[0];
            tremoloTable[1][counter] = tremoloTable[1][counter - 1] + tremoloIncrement[1];
        }
        // The second half of the triangle waveform:
        while (tremoloTable[0][counter] > tremoloDepth[0] && counter < tremoloTableLength - 1) {
            counter++;
            tremoloTable[0][counter] = tremoloTable[0][counter - 1] - tremoloIncrement[0];
            tremoloTable[1][counter] = tremoloTable[1][counter - 1] - tremoloIncrement[1];
        }

        return tremoloTable;
    })();

    return OPL3Data;
})();






var OperatorData = (function() {

    function OperatorData() {
    }

    OperatorData.AM1_VIB1_EGT1_KSR1_MULT4_Offset = 0x20; // int
    OperatorData.KSL2_TL6_Offset = 0x40; // int
    OperatorData.AR4_DR4_Offset = 0x60; // int
    OperatorData.SL4_RR4_Offset = 0x80; // int
    OperatorData._5_WS3_Offset = 0xE0; // int

    OperatorData.type = {
        NO_MODULATION : 'NO_MODULATION',
        CARRIER : 'CARRIER',
        FEEDBACK : 'FEEDBACK'
    }; // doesn't seem to be used.

    OperatorData.waveLength = 1024;
    OperatorData.multTable = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 10.0, 12.0, 12.0, 15.0, 15.0];
    OperatorData.ksl3dBtable = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, -3, -6, -9],
        [0, 0, 0, 0, -3, -6, -9, -12],
        [0, 0, 0, -1.875, -4.875, -7.875, -10.875, -13.875],

        [0, 0, 0, -3, -6, -9, -12, -15],
        [0, 0, -1.125, -4.125, -7.125, -10.125, -13.125, -16.125],
        [0, 0, -1.875, -4.875, -7.875, -10.875, -13.875, -16.875],
        [0, 0, -2.625, -5.625, -8.625, -11.625, -14.625, -17.625],

        [0, 0, -3, -6, -9, -12, -15, -18],
        [0, -0.750, -3.750, -6.750, -9.750, -12.750, -15.750, -18.750],
        [0, -1.125, -4.125, -7.125, -10.125, -13.125, -16.125, -19.125],
        [0, -1.500, -4.500, -7.500, -10.500, -13.500, -16.500, -19.500],

        [0, -1.875, -4.875, -7.875, -10.875, -13.875, -16.875, -19.875],
        [0, -2.250, -5.250, -8.250, -11.250, -14.250, -17.250, -20.250],
        [0, -2.625, -5.625, -8.625, -11.625, -14.625, -17.625, -20.625],
        [0, -3, -6, -9, -12, -15, -18, -21]
    ];

    OperatorData.waveforms = function() { // replaces "loadWaveforms" static method
        var i;
        var waveforms = OPL3JSUtil.initArray(8, null); // double[]
        for (i = 0; i < 8; i++) {
            waveforms[i] = OPL3JSUtil.initArray(OperatorData.waveLength, 0);
        }

        // 1st waveform: sinusoid.
        var theta = 0;
        var thetaIncrement = 2.0 * Math.PI / 1024.0; // double

        // TODO: replace usage of 1024, 768, 512, 256 etc with divisions of OperatorData.waveLength.
        for (i = 0, theta = 0; i < 1024; i++, theta += thetaIncrement) {
            waveforms[0][i] = Math.sin(theta);
        }

        var sineTable = waveforms[0]; // double[]
        // 2nd: first half of a sinusoid.
        for (i = 0; i < 512; i++) {
            waveforms[1][i] = sineTable[i];
            waveforms[1][512 + i] = 0;
        }
        // 3rd: double positive sinusoid.
        for (i = 0; i < 512; i++)
            waveforms[2][i] = waveforms[2][512 + i] = sineTable[i];
        // 4th: first and third quarter of double positive sinusoid.
        for (i = 0; i < 256; i++) {
            waveforms[3][i] = waveforms[3][512 + i] = sineTable[i];
            waveforms[3][256 + i] = waveforms[3][768 + i] = 0;
        }
        // 5th: first half with double frequency sinusoid.
        for (i = 0; i < 512; i++) {
            waveforms[4][i] = sineTable[i * 2];
            waveforms[4][512 + i] = 0;
        }
        // 6th: first half with double frequency positive sinusoid.
        for (i=0; i < 256; i++) {
            waveforms[5][i] = waveforms[5][256 + i] = sineTable[i * 2];
            waveforms[5][512 + i] = waveforms[5][768 + i] = 0;
        }
        // 7th: square wave
        for (i=0; i < 512; i++) {
            waveforms[6][i] = 1;
            waveforms[6][512 + i] = -1;
        }
        // 8th: exponential
        var x; // double
        var xIncrement = 1 * 16.0 / 256.0; // double
        for (i = 0, x = 0; i < 512; i++, x += xIncrement) {
            waveforms[7][i] = Math.pow(2, -x);
            waveforms[7][1023 - i] = -Math.pow(2, -(x + 1 / 16.0));
        }

        return waveforms;
    }();

    OperatorData.log2 = function(x) {
        return Math.log(x) / Math.log(2);
    };

    return OperatorData;
})();

var PhaseGenerator;

PhaseGenerator = (function() {
	function PhaseGenerator(opl3Chip) {
        this.opl3Chip = opl3Chip;
        this.phase = this.phaseIncrement = 0;
	}

	PhaseGenerator.prototype.setFrequency = function(f_number, block, mult) {
		// This frequency formula is derived from the following equation:
		// f_number = baseFrequency * pow(2,19) / sampleRate / pow(2,block-1);
		var baseFrequency =
			f_number * Math.pow(2, block - 1) * OPL3Data.sampleRate / Math.pow(2, 19);
		var operatorFrequency = baseFrequency * OperatorData.multTable[mult];

		// phase goes from 0 to 1 at
		// period = (1/frequency) seconds ->
		// Samples in each period is (1/frequency)*sampleRate =
		// = sampleRate/frequency ->
		// So the increment in each sample, to go from 0 to 1, is:
		// increment = (1-0) / samples in the period ->
		// increment = 1 / (OPL3Data.sampleRate/operatorFrequency) ->
		this.phaseIncrement = operatorFrequency / OPL3Data.sampleRate;
	};

	PhaseGenerator.prototype.getPhase = function(vib) {
		if (vib == 1) {
			// phaseIncrement = (operatorFrequency * vibrato) / sampleRate
			this.phase += this.phaseIncrement * OPL3Data.vibratoTable[this.opl3Chip.dvb][this.opl3Chip.vibratoIndex];
		} else {
			// phaseIncrement = operatorFrequency / sampleRate
			this.phase += this.phaseIncrement;
		}
		this.phase %= 1;
		return this.phase;
	};

	PhaseGenerator.prototype.keyOn = function() {
		this.phase = 0;
	};

	PhaseGenerator.prototype.toString = function() {
		return 'Operator frequency: ' + OPL3Data.sampleRate * this.phaseIncrement + ' Hz.\n';
	};

    return PhaseGenerator;
})();



var EnvelopeGeneratorData = (function() {
    function EnvelopeGeneratorData() {
    }

    EnvelopeGeneratorData.INFINITY = 1.0 / 0.0; // hmm
    // This table is indexed by the value of Operator.ksr
    // and the value of ChannelRegister.keyScaleNumber.
        EnvelopeGeneratorData.rateOffset = [
        [0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3],
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    ];
    // These attack periods in miliseconds were taken from the YMF278B manual.
    // The attack actual rates range from 0 to 63, with different data for
    // 0%-100% and for 10%-90%:
    EnvelopeGeneratorData.attackTimeValuesTable = [
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [2826.24, 1482.75], [2252.80, 1155.07], [1884.16, 991.23], [1597.44, 868.35],
        [1413.12, 741.38], [1126.40, 577.54], [942.08, 495.62], [798.72, 434.18],
        [706.56, 370.69], [563.20, 288.77], [471.04, 247.81], [399.36, 217.09],

        [353.28, 185.34], [281.60, 144.38], [235.52, 123.90], [199.68, 108.54],
        [176.76, 92.67], [140.80, 72.19], [117.76, 61.95], [99.84, 54.27],
        [88.32, 46.34], [70.40, 36.10], [58.88, 30.98], [49.92, 27.14],
        [44.16, 23.17], [35.20, 18.05], [29.44, 15.49], [24.96, 13.57],

        [22.08, 11.58], [17.60, 9.02], [14.72, 7.74], [12.48, 6.78],
        [11.04, 5.79], [8.80, 4.51], [7.36, 3.87], [6.24, 3.39],
        [5.52, 2.90], [4.40, 2.26], [3.68, 1.94], [3.12, 1.70],
        [2.76, 1.45], [2.20, 1.13], [1.84, 0.97], [1.56, 0.85],

        [1.40, 0.73], [1.12, 0.61], [0.92, 0.49], [0.80, 0.43],
        [0.70, 0.37], [0.56, 0.31], [0.46, 0.26], [0.42, 0.22],
        [0.38, 0.19], [0.30, 0.14], [0.24, 0.11], [0.20, 0.11],
        [0.00, 0.00], [0.00, 0.00], [0.00, 0.00], [0.00, 0.00]
    ];

    EnvelopeGeneratorData.decayAndReleaseTimeValuesTable = [
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [EnvelopeGeneratorData.INFINITY, EnvelopeGeneratorData.INFINITY],
        [39280.64, 8212.48], [31416.32, 6574.08], [26173.44, 5509.12], [22446.08, 4730.88],
        [19640.32, 4106.24], [15708.16, 3287.04], [13086.72, 2754.56], [11223.04, 2365.44],
        [9820.16, 2053.12], [7854.08, 1643.52], [6543.36, 1377.28], [5611.52, 1182.72],

        [4910.08, 1026.56], [3927.04, 821.76], [3271.68, 688.64], [2805.76, 591.36],
        [2455.04, 513.28], [1936.52, 410.88], [1635.84, 344.34], [1402.88, 295.68],
        [1227.52, 256.64], [981.76, 205.44], [817.92, 172.16], [701.44, 147.84],
        [613.76, 128.32], [490.88, 102.72], [488.96, 86.08], [350.72, 73.92],

        [306.88, 64.16], [245.44, 51.36], [204.48, 43.04], [175.36, 36.96],
        [153.44, 32.08], [122.72, 25.68], [102.24, 21.52], [87.68, 18.48],
        [76.72, 16.04], [61.36, 12.84], [51.12, 10.76], [43.84, 9.24],
        [38.36, 8.02], [30.68, 6.42], [25.56, 5.38], [21.92, 4.62],

        [19.20, 4.02], [15.36, 3.22], [12.80, 2.68], [10.96, 2.32],
        [9.60, 2.02], [7.68, 1.62], [6.40, 1.35], [5.48, 1.15],
        [4.80, 1.01], [3.84, 0.81], [3.20, 0.69], [2.74, 0.58],
        [2.40, 0.51], [2.40, 0.51], [2.40, 0.51], [2.40, 0.51]
    ];

    return EnvelopeGeneratorData;
})();


var EnvelopeGenerator;

EnvelopeGenerator = (function() {
    function EnvelopeGenerator(opl3Chip) {
        this.opl3Chip = opl3Chip;
        this.stage = EnvelopeGenerator.Stage.OFF;
        this.actualAttackRate = this.actualDecayRate = this.actualReleaseRate = 0; // int
        this.xAttackIncrement = this.xMinimumInAttack = 0.0; // double
        this.dBdecayIncrement = 0.0; // double
        this.dBreleaseIncrement = 0.0; // double
        this.attenuation = this.totalLevel = this.sustainLevel = 0.0; // double
        this.x = EnvelopeGenerator._dBtoX(-96.0); // double
        this.envelope = -96.0; // double
    }

    // ########
    // Static properties
    // ########
    //EnvelopeGenerator.INFINITY = null; // double[], hmm. not used.
    EnvelopeGenerator.Stage = {
        ATTACK : 'ATTACK',
        DECAY : 'DECAY',
        SUSTAIN : 'SUSTAIN',
        RELEASE : 'RELEASE',
        OFF : 'OFF'
    }; // enum

    // The next three methods are static and private.
    EnvelopeGenerator._dBtoX = function(dB) {
        return OperatorData.log2(-dB); // double
    };

    EnvelopeGenerator._percentageToDB = function(percentage) {
        // Hack. Not sure how to get around differences in rounding errors.
        // The Java implementation calls Math.log10(), which does not suffer from
        //  the same rounding error(s).
        // Note that a different implementation could be:
        // (Math.log(percentage) * Math.LOG10E)
        //  ... but this gives different rounding errors again.
        if (percentage === 0.1) {
            return -10.0;
        }
        return ((Math.log(percentage) / Math.LN10) * 10.0);
    };

    EnvelopeGenerator._percentageToX = function(percentage) {
        return EnvelopeGenerator._dBtoX(EnvelopeGenerator._percentageToDB(percentage)); // double
    };

    // ########
    // Public methods
    // ########
    EnvelopeGenerator.prototype.setActualSustainLevel = function(sl) {
        // If all SL bits are 1, sustain level is set to -93 dB:
        if (sl == 0x0F) {
            this.sustainLevel = -93;
            return;
        }
        // The datasheet states that the SL formula is
        // sustainLevel = -24*d7 -12*d6 -6*d5 -3*d4,
        // translated as:
        this.sustainLevel = -3 * sl;
    };

    EnvelopeGenerator.prototype.setTotalLevel = function(tl) {
        // The datasheet states that the TL formula is
        // TL = -(24*d5 + 12*d4 + 6*d3 + 3*d2 + 1.5*d1 + 0.75*d0),
        // translated as:
        this.totalLevel = tl * -0.75;
    };

    EnvelopeGenerator.prototype.setAtennuation = function(f_number, block, ksl) {
        var hi4bits = (f_number >> 6) & 0x0F;
        switch (ksl) {
            case 0:
                this.attenuation = 0;
                break;
            case 1:
                // ~3 dB/Octave
                this.attenuation = OperatorData.ksl3dBtable[hi4bits][block];
                break;
            case 2:
                // ~1.5 dB/Octave
                this.attenuation = OperatorData.ksl3dBtable[hi4bits][block] / 2;
                break;
            case 3:
                // ~6 dB/Octave
                this.attenuation = OperatorData.ksl3dBtable[hi4bits][block] * 2;
        }
    };

    EnvelopeGenerator.prototype.setActualAttackRate = function(attackRate, ksr, keyScaleNumber) {
        // According to the YMF278B manual's OPL3 section, the attack curve is exponential,
        // with a dynamic range from -96 dB to 0 dB and a resolution of 0.1875 dB
        // per level.
        //
        // This method sets an attack increment and attack minimum value
        // that creates a exponential dB curve with 'period0to100' seconds in length
        // and 'period10to90' seconds between 10% and 90% of the curve total level.
        this.actualAttackRate = this._calculateActualRate(attackRate, ksr, keyScaleNumber);
        var period0to100inSeconds = EnvelopeGeneratorData.attackTimeValuesTable[this.actualAttackRate][0] / 1000.0; // double
        var period0to100inSamples = OPL3JSUtil.castToJavaInt((period0to100inSeconds * OPL3Data.sampleRate)); // int
        var period10to90inSeconds = EnvelopeGeneratorData.attackTimeValuesTable[this.actualAttackRate][1] / 1000.0; // double
        var period10to90inSamples = OPL3JSUtil.castToJavaInt((period10to90inSeconds * OPL3Data.sampleRate)); // int
        // The x increment is dictated by the period between 10% and 90%:
        this.xAttackIncrement = OPL3Data.calculateIncrement(EnvelopeGenerator._percentageToX(0.1), EnvelopeGenerator._percentageToX(0.9), period10to90inSeconds);
        // Discover how many samples are still from the top.
        // It cannot reach 0 dB, since x is a logarithmic parameter and would be
        // negative infinity. So we will use -0.1875 dB as the resolution
        // maximum.
        //
        // percentageToX(0.9) + samplesToTheTop*xAttackIncrement = dBToX(-0.1875); ->
        // samplesToTheTop = (dBtoX(-0.1875) - percentageToX(0.9)) / xAttackIncrement); ->
        // period10to100InSamples = period10to90InSamples + samplesToTheTop; ->
        // Need to parse "period10to90inSamples" to a float, since it could be "Infinity", and Javascript
        //  will treat the addition as a string concatenation.
        var period10to100inSamples = (period10to90inSamples + (EnvelopeGenerator._dBtoX(-0.1875) - EnvelopeGenerator._percentageToX(0.9)) / this.xAttackIncrement);
        period10to100inSamples = OPL3JSUtil.castToJavaInt(period10to100inSamples); // int
        // Discover the minimum x that, through the attackIncrement value, keeps
        // the 10%-90% period, and reaches 0 dB at the total period:
        this.xMinimumInAttack = EnvelopeGenerator._percentageToX(0.1) - (period0to100inSamples - period10to100inSamples) * this.xAttackIncrement;
    };

    EnvelopeGenerator.prototype.setActualDecayRate = function(decayRate, ksr, keyScaleNumber) {
        this.actualDecayRate = this._calculateActualRate(decayRate, ksr, keyScaleNumber);
        var period10to90inSeconds = EnvelopeGeneratorData.decayAndReleaseTimeValuesTable[this.actualDecayRate][1] / 1000.0; // double
        // Differently from the attack curve, the decay/release curve is linear.
        // The dB increment is dictated by the period between 10% and 90%:
        this.dBdecayIncrement = OPL3Data.calculateIncrement(EnvelopeGenerator._percentageToDB(0.1), EnvelopeGenerator._percentageToDB(0.9), period10to90inSeconds);
    };

    EnvelopeGenerator.prototype.setActualReleaseRate = function(releaseRate, ksr, keyScaleNumber) {
        this.actualReleaseRate = this._calculateActualRate(releaseRate, ksr, keyScaleNumber);
        var period10to90inSeconds = EnvelopeGeneratorData.decayAndReleaseTimeValuesTable[this.actualReleaseRate][1] / 1000.0; // double
        this.dBreleaseIncrement = OPL3Data.calculateIncrement(EnvelopeGenerator._percentageToDB(0.1), EnvelopeGenerator._percentageToDB(0.9), period10to90inSeconds);
    };

    EnvelopeGenerator.prototype.getEnvelope = function(egt, am) {
        // The datasheets attenuation values
        // must be halved to match the real OPL3 output.
        var envelopeSustainLevel = this.sustainLevel / 2.0; // double
        var envelopeTremolo =
            OPL3Data.tremoloTable[this.opl3Chip.dam][this.opl3Chip.tremoloIndex] / 2.0; // double
        var envelopeAttenuation = this.attenuation / 2.0; // double
        var envelopeTotalLevel = this.totalLevel / 2.0; // double

        var envelopeMinimum = -96.0; // double
        var envelopeResolution = 0.1875; // double

        var outputEnvelope; // double
        //
        // Envelope Generation
        //
        switch (this.stage) {
            case EnvelopeGenerator.Stage.ATTACK:
                // Since the attack is exponential, it will never reach 0 dB, so
                // we´ll work with the next to maximum in the envelope resolution.
                if (this.envelope < -envelopeResolution && this.xAttackIncrement != -EnvelopeGeneratorData.INFINITY) {
                    // The attack is exponential.
                    this.envelope = -Math.pow(2, this.x);
                    this.x += this.xAttackIncrement;
                    break;
                } else {
                    // It is needed here to explicitly set envelope = 0, since
                    // only the attack can have a period of
                    // 0 seconds and produce an infinity envelope increment.
                    this.envelope = 0;
                    this.stage = EnvelopeGenerator.Stage.DECAY;
                }
            case EnvelopeGenerator.Stage.DECAY:
                // The decay and release are linear.
                if (this.envelope > envelopeSustainLevel) {
                    this.envelope -= this.dBdecayIncrement;
                    break;
                } else {
                    this.stage = EnvelopeGenerator.Stage.SUSTAIN;
                }
            case EnvelopeGenerator.Stage.SUSTAIN:
                // The Sustain stage is mantained all the time of the Key ON,
                // even if we are in non-sustaining mode.
                // This is necessary because, if the key is still pressed, we can
                // change back and forth the state of EGT, and it will release and
                // hold again accordingly.
                if (egt == 1) {
                    break;
                } else {
                    if (this.envelope > envelopeMinimum) {
                        this.envelope -= this.dBreleaseIncrement;
                    } else {
                        this.stage = EnvelopeGenerator.Stage.OFF;
                    }
                }
                break;
            case EnvelopeGenerator.Stage.RELEASE:
                // If we have Key OFF, only here we are in the Release stage.
                // Now, we can turn EGT back and forth and it will have no effect,i.e.,
                // it will release inexorably to the Off stage.
                if (this.envelope > envelopeMinimum) {
                    this.envelope -= this.dBreleaseIncrement;
                } else {
                    this.stage = EnvelopeGenerator.Stage.OFF;
                }
        }

        // Ongoing original envelope
        outputEnvelope = this.envelope;

        //Tremolo
        if (am == 1) {
            outputEnvelope += envelopeTremolo;
        }

        //Attenuation
        outputEnvelope += envelopeAttenuation;

        //Total Level
        outputEnvelope += envelopeTotalLevel;

        return outputEnvelope;
    };

    EnvelopeGenerator.prototype.keyOn = function() {
        // If we are taking it in the middle of a previous envelope,
        // start to rise from the current level:
        // envelope = - (2 ^ x); ->
        // 2 ^ x = -envelope ->
        // x = log2(-envelope); ->
        var xCurrent = OperatorData.log2(-this.envelope); // double
        this.x = xCurrent < this.xMinimumInAttack ? xCurrent : this.xMinimumInAttack;
        this.stage = EnvelopeGenerator.Stage.ATTACK;
    };

    EnvelopeGenerator.prototype.keyOff = function() {
        if (this.stage != EnvelopeGenerator.Stage.OFF) {
            this.stage = EnvelopeGenerator.Stage.RELEASE;
        }
    };

    EnvelopeGenerator.prototype.toString = function() {
        var str = '';
        str += 'Envelope Generator: \n';
        var attackPeriodInSeconds = EnvelopeGeneratorData.attackTimeValuesTable[this.actualAttackRate][0] / 1000.0;
        str += '\tATTACK  ' + attackPeriodInSeconds + ' s, rate ' + this.actualAttackRate + '. \n';
        var decayPeriodInSeconds = EnvelopeGeneratorData.decayAndReleaseTimeValuesTable[this.actualDecayRate][0] / 1000.0;
        str += '\tDECAY   ' + decayPeriodInSeconds + ' s, rate ' + this.actualDecayRate + '. \n';
        str += '\tSL      ' + this.sustainLevel + ' dB. \n';
        var releasePeriodInSeconds = EnvelopeGeneratorData.decayAndReleaseTimeValuesTable[this.actualReleaseRate][0] / 1000.0;
        str += '\tRELEASE ' + releasePeriodInSeconds + ' s, rate ' + this.actualReleaseRate + '. \n';
        str += '\n';
        return str;
    };

    // ########
    // Private methods
    // ########
    EnvelopeGenerator.prototype._calculateActualRate = function(rate, ksr, keyScaleNumber) {
        var rof = EnvelopeGeneratorData.rateOffset[ksr][keyScaleNumber]; // int
        var actualRate = rate * 4 + rof; // int
        // If, as an example at the maximum, rate is 15 and the rate offset is 15,
        // the value would
        // be 75, but the maximum allowed is 63:
        if (actualRate > 63) {
            actualRate = 63;
        }
        return actualRate;
    };

    return EnvelopeGenerator;
})();


var Operator;

Operator = (function() {
    function Operator(opl3Chip, baseAddress) {
        this.opl3Chip = opl3Chip;
        this.operatorBaseAddress = baseAddress;
        this.phaseGenerator = new PhaseGenerator(opl3Chip);
        this.envelopeGenerator = new EnvelopeGenerator(opl3Chip);

        this.envelope = 0.0;
        this.phase = 0.0;
        this.am = this.vib = this.ksr = this.egt = this.mult = this.ksl = this.tl = this.ar = this.dr = this.sl = this.rr = this.ws = 0;
        this.keyScaleNumber = this.f_number = this.block = 0;
    }

    // ########
    // Static properties
    // ########
    Operator.noModulator = 0;

    // ########
    // Public methods
    // ########
    Operator.prototype.update_AM1_VIB1_EGT1_KSR1_MULT4 = function() {
        var am1_vib1_egt1_ksr1_mult4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.AM1_VIB1_EGT1_KSR1_MULT4_Offset];

        // Amplitude Modulation. This register is used int EnvelopeGenerator.getEnvelope();
        this.am  = (am1_vib1_egt1_ksr1_mult4 & 0x80) >> 7;
        // Vibrato. This register is used in PhaseGenerator.getPhase();
        this.vib = (am1_vib1_egt1_ksr1_mult4 & 0x40) >> 6;
        // Envelope Generator Type. This register is used in EnvelopeGenerator.getEnvelope();
        this.egt = (am1_vib1_egt1_ksr1_mult4 & 0x20) >> 5;
        // Key Scale Rate. Sets the actual envelope rate together with rate and keyScaleNumber.
        // This register os used in EnvelopeGenerator.setActualAttackRate().
        this.ksr = (am1_vib1_egt1_ksr1_mult4 & 0x10) >> 4;
        // Multiple. Multiplies the Channel.baseFrequency to get the Operator.operatorFrequency.
        // This register is used in PhaseGenerator.setFrequency().
        this.mult = am1_vib1_egt1_ksr1_mult4 & 0x0F;

        this.phaseGenerator.setFrequency(this.f_number, this.block, this.mult);
        this.envelopeGenerator.setActualAttackRate(this.ar, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualDecayRate(this.dr, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualReleaseRate(this.rr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_KSL2_TL6 = function() {
        var ksl2_tl6 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.KSL2_TL6_Offset];

        // Key Scale Level. Sets the attenuation in accordance with the octave.
        this.ksl = (ksl2_tl6 & 0xC0) >> 6;
        // Total Level. Sets the overall damping for the envelope.
        this.tl =  ksl2_tl6 & 0x3F;

        this.envelopeGenerator.setAtennuation(this.f_number, this.block, this.ksl);
        this.envelopeGenerator.setTotalLevel(this.tl);
    };

    Operator.prototype.update_AR4_DR4 = function() {
        var ar4_dr4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.AR4_DR4_Offset];

        // Attack Rate.
        this.ar = (ar4_dr4 & 0xF0) >> 4;
        // Decay Rate.
        this.dr =  ar4_dr4 & 0x0F;

        this.envelopeGenerator.setActualAttackRate(this.ar, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualDecayRate(this.dr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_SL4_RR4 = function() {
        var sl4_rr4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.SL4_RR4_Offset];

        // Sustain Level.
        this.sl = (sl4_rr4 & 0xF0) >> 4;
        // Release Rate.
        this.rr = sl4_rr4 & 0x0F;

        this.envelopeGenerator.setActualSustainLevel(this.sl);
        this.envelopeGenerator.setActualReleaseRate(this.rr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_5_WS3 = function() {
        var _5_ws3 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData._5_WS3_Offset];
        this.ws =  _5_ws3 & 0x07;
    };

    Operator.prototype.getOperatorOutput = function(modulator) {
        if (this.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF) {
            return 0;
        }

        var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
        this.envelope = Math.pow(10, envelopeInDB / 10.0);

        // If it is in OPL2 mode, use first four waveforms only:
        this.ws &= ((this.opl3Chip._new << 2) + 3);
        var waveform = OperatorData.waveforms[this.ws]; // double[]

        this.phase = this.phaseGenerator.getPhase(this.vib);

        var operatorOutput = this._getOutput(modulator, this.phase, waveform); // double
        return operatorOutput;
    };

    Operator.prototype.toString = function() {
        var str = '';
        var operatorFrequency = this.f_number * Math.pow(2, this.block - 1) * OPL3Data.sampleRate / Math.pow(2, 19) * OperatorData.multTable[this.mult];
        str += 'operatorBaseAddress: ' + this.operatorBaseAddress + '\n';
        str += 'operatorFrequency: ' + operatorFrequency + '\n';
        str += 'mult: ' + this.mult;
        str += 'ar: ' + this.ar;
        str += 'dr: ' + this.dr;
        str += 'sl: ' + this.sl;
        str += 'rr: ' + this.rr;
        str += 'ws: ' + this.ws + '\n';
        str += 'am: ' + this.am;
        str += 'vib: ' + this.vib;
        str += 'ksr: ' + this.ksr;
        str += 'egt: ' + this.egt;
        str += 'ksl: ' + this.ksl;
        str += 'tl: ' + this.tl + '\n';
        return str;
    };

    // ########
    // Protected methods
    // ########
    Operator.prototype._getOutput = function(modulator, outputPhase, waveform) {
        outputPhase = (outputPhase + modulator) % 1;
        if (outputPhase < 0) {
            outputPhase++;
            // If the double could not afford to be less than 1:
            outputPhase %= 1;
        }
        var sampleIndex = parseInt(outputPhase * OperatorData.waveLength);
        return waveform[sampleIndex] * this.envelope;
    };

    Operator.prototype._keyOn = function() {
        if (this.ar > 0) {
            this.envelopeGenerator.keyOn();
            this.phaseGenerator.keyOn();
        }
        else {
            this.envelopeGenerator.stage = EnvelopeGenerator.Stage.OFF;
        }
    };

    Operator.prototype._keyOff = function() {
        this.envelopeGenerator.keyOff();
    };

    Operator.prototype._updateOperator = function(ksn, f_num, blk) {
        this.keyScaleNumber = ksn;
        this.f_number = f_num;
        this.block = blk;
        this.update_AM1_VIB1_EGT1_KSR1_MULT4();
        this.update_KSL2_TL6();
        this.update_AR4_DR4();
        this.update_SL4_RR4();
        this.update_5_WS3();
    };

    return Operator;
})();








var TopCymbalOperator;

TopCymbalOperator = (function(_super) {

    OPL3JSUtil.__extends(TopCymbalOperator, _super);

	function TopCymbalOperator(opl3Chip, baseAddress) {
		if (baseAddress === undefined) {
			baseAddress = TopCymbalOperator.topCymbalOperatorBaseAddress;
		}
		return TopCymbalOperator.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
	}

	TopCymbalOperator.topCymbalOperatorBaseAddress = 0x15;

	// This method is used here with the HighHatOperator phase
	// as the externalPhase.
	// Conversely, this method is also used through inheritance by the HighHatOperator,
	// now with the TopCymbalOperator phase as the externalPhase.
	TopCymbalOperator.prototype.getOperatorOutput = function(modulator, externalPhase) {
		if (externalPhase === undefined) {
			// The Top Cymbal operator uses his own phase together with the High Hat phase.
			externalPhase = this.opl3Chip.highHatOperator.phase * OperatorData.multTable[this.opl3Chip.highHatOperator.mult]; // double
		}
		var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
		this.envelope = Math.pow(10, envelopeInDB / 10.0);

		this.phase = this.phaseGenerator.getPhase(this.vib);

		var waveIndex = this.ws & ((this.opl3Chip._new << 2) + 3); // int
		var waveform = OperatorData.waveforms[waveIndex]; // double[]

		// Empirically tested multiplied phase for the Top Cymbal:
		var carrierPhase = (8 * this.phase) % 1; // double
		var modulatorPhase = externalPhase; // double
		var modulatorOutput = this._getOutput(Operator.noModulator, modulatorPhase, waveform); // double
		var carrierOutput = this._getOutput(modulatorOutput, carrierPhase, waveform); // double

		var cycles = 4; // int
		if((carrierPhase * cycles) % cycles > 0.1) {
			carrierOutput = 0;
		}

		return carrierOutput * 2;
	};

    return TopCymbalOperator;
})(Operator);


var HighHatOperator;

HighHatOperator = (function(_super) {

    OPL3JSUtil.__extends(HighHatOperator, _super);

	function HighHatOperator(opl3Chip) {
		return HighHatOperator.__super__.constructor.apply(this, [
            opl3Chip,
            HighHatOperator.highHatOperatorBaseAddress
        ]);
	}

	HighHatOperator.highHatOperatorBaseAddress = 0x11;

	HighHatOperator.prototype.getOperatorOutput = function(modulator) {
		var topCymbalOperatorPhase =
			this.opl3Chip.topCymbalOperator.phase * OperatorData.multTable[this.opl3Chip.topCymbalOperator.mult];
		// The sound output from the High Hat resembles the one from
		// Top Cymbal, so we use the parent method and modifies his output
		// accordingly afterwards.
		var operatorOutput = HighHatOperator.__super__.getOperatorOutput.call(this, modulator, topCymbalOperatorPhase);
		if (operatorOutput == 0) {
            operatorOutput = Math.random() * this.envelope;
        }
		return operatorOutput;
	};

    return HighHatOperator;
})(TopCymbalOperator);





var SnareDrumOperator;

SnareDrumOperator = (function(_super) {

    OPL3JSUtil.__extends(SnareDrumOperator, _super);

	function SnareDrumOperator(opl3Chip) {
		return SnareDrumOperator.__super__.constructor.apply(this, [
            opl3Chip,
            SnareDrumOperator.snareDrumOperatorBaseAddress
        ]);
	}

	SnareDrumOperator.snareDrumOperatorBaseAddress = 0x14;

	SnareDrumOperator.prototype.getOperatorOutput = function(modulator) {
		if (this.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF) {
			return 0;
		}

		var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
		this.envelope = Math.pow(10, envelopeInDB / 10.0);

		// If it is in OPL2 mode, use first four waveforms only:
		var waveIndex = this.ws & ((this.opl3Chip._new << 2) + 3); // int
		var waveform = OperatorData.waveforms[waveIndex]; // double[]

		this.phase = this.opl3Chip.highHatOperator.phase * 2;

		var operatorOutput = this._getOutput(modulator, this.phase, waveform); // double

		var noise = Math.random() * this.envelope; // double

		if (operatorOutput / this.envelope != 1 && operatorOutput / this.envelope != -1) {
			if (operatorOutput > 0) {
				operatorOutput = noise;
			} else if (operatorOutput < 0) {
				operatorOutput = -noise;
			} else {
				operatorOutput = 0;
			}
		}
		return operatorOutput * 2;
	};

    return SnareDrumOperator;
})(Operator);



var TomTomOperator;

TomTomOperator = (function(_super) {

    OPL3JSUtil.__extends(TomTomOperator, _super);

	function TomTomOperator(opl3Chip) {
		return TomTomOperator.__super__.constructor.apply(this, [
            opl3Chip,
            TomTomOperator.tomTomOperatorBaseAddress
        ]);
	}

	TomTomOperator.tomTomOperatorBaseAddress = 0x12;

    return TomTomOperator;
})(Operator);





var ChannelData = (function () {
    function ChannelData() {
    }

    ChannelData._2_KON1_BLOCK3_FNUMH2_Offset = 0xB0;
    ChannelData.FNUML8_Offset = 0xA0;
    ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset = 0xC0;
    // Feedback rate in fractions of 2*Pi, normalized to (0,1):
    // 0, Pi/16, Pi/8, Pi/4, Pi/2, Pi, 2*Pi, 4*Pi turns to be:
    ChannelData.feedback = [0, 1.0 / 32.0, 1.0 / 16.0, 1.0 / 8.0, 1.0 / 4.0, 1.0 / 2.0, 1, 2];

    return ChannelData;
})();

var Channel;

Channel = (function() {
    function Channel(opl3Chip, baseAddress) {
        this.opl3Chip = opl3Chip;
        this.channelBaseAddress = baseAddress; // int
        this.feedback = OPL3JSUtil.initArray(2, 0.0); // double[]
        (this.fnuml = this.fnumh = this.kon = this.block =
            this.cha = this.chb = this.chc = this.chd = this.fb = this.cnt = 0);
        this.output = OPL3JSUtil.initArray(4, 0.0); // double[]
    }

    // Factor to convert between normalized amplitude to normalized
    // radians. The amplitude maximum is equivalent to 8*Pi radians.
    Channel.toPhase = 4;

    Channel.prototype.update_2_KON1_BLOCK3_FNUMH2 = function() {
        var _2_kon1_block3_fnumh2 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData._2_KON1_BLOCK3_FNUMH2_Offset];

        // Frequency Number (hi-register) and Block. These two registers, together with fnuml,
        // sets the Channel´s base frequency;
        this.block = (_2_kon1_block3_fnumh2 & 0x1C) >> 2;
        this.fnumh = _2_kon1_block3_fnumh2 & 0x03;
        this._updateOperators();

        // Key On. If changed, calls Channel.keyOn() / keyOff().
        var newKon   = (_2_kon1_block3_fnumh2 & 0x20) >> 5; // int
        if (newKon != this.kon) {
            if (newKon == 1) {
                this._keyOn();
            } else {
                this._keyOff();
            }
            this.kon = newKon;
        }
    };

    Channel.prototype.update_FNUML8 = function() {
        var fnuml8 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData.FNUML8_Offset];
        // Frequency Number, low register.
        this.fnuml = fnuml8 & 0xFF;
        this._updateOperators();
    };

    Channel.prototype.update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1 = function() {
        var chd1_chc1_chb1_cha1_fb3_cnt1 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset];
        this.chd = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x80) >> 7;
        this.chc = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x40) >> 6;
        this.chb = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x20) >> 5;
        this.cha = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x10) >> 4;
        this.fb = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x0E) >> 1;
        this.cnt = chd1_chc1_chb1_cha1_fb3_cnt1 & 0x01;
        this._updateOperators();
    };

    Channel.prototype.updateChannel = function() {
        this.update_2_KON1_BLOCK3_FNUMH2();
        this.update_FNUML8();
        this.update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
    };

    Channel.prototype._getInFourChannels = function(channelOutput) {
        var output = this.output;
        OPL3JSUtil.fillArray(output, 0.0); // double[]

        if (this.opl3Chip._new === 0) {
            output[0] = output[1] = output[2] = output[3] = channelOutput;
        } else {
            output[0] = (this.cha == 1) ? channelOutput : 0;
            output[1] = (this.chb == 1) ? channelOutput : 0;
            output[2] = (this.chc == 1) ? channelOutput : 0;
            output[3] = (this.chd == 1) ? channelOutput : 0;
        }

        return output;
    };

    // Abstract methods
    Channel.prototype.getChannelOutput = function() {
        // Override this method and return double[]
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._keyOn = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._keyOff = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._updateOperators = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    return Channel;
})();



var Channel2op;

Channel2op = (function(_super) {

	OPL3JSUtil.__extends(Channel2op, _super);

	function Channel2op(opl3Chip, baseAddress, o1, o2) {
		this.op1 = o1;
		this.op2 = o2;
		return Channel2op.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
	}

	Channel2op.prototype.getChannelOutput = function() {
		var channelOutput = 0; // double
		var op1Output = 0; // double
		var op2Output = 0; // double
		var output; // double[]
		// The feedback uses the last two outputs from
		// the first operator, instead of just the last one.
		var feedbackOutput = (this.feedback[0] + this.feedback[1]) / 2; // double

		switch (this.cnt) {
			// CNT = 0, the operators are in series, with the first in feedback.
			case 0:
				if (this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
					return this._getInFourChannels(0);
				op1Output = this.op1.getOperatorOutput(feedbackOutput);
				channelOutput = this.op2.getOperatorOutput(op1Output * Channel.toPhase);
				break;
			// CNT = 1, the operators are in parallel, with the first in feedback.
			case 1:
				if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
					this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
					return this._getInFourChannels(0);
				op1Output = this.op1.getOperatorOutput(feedbackOutput);
				op2Output = this.op2.getOperatorOutput(Operator.noModulator);
				channelOutput = (op1Output + op2Output) / 2;
				break;
		}

		this.feedback[0] = this.feedback[1];
		this.feedback[1] = (op1Output * ChannelData.feedback[this.fb]) % 1;
		output = this._getInFourChannels(channelOutput);
		return output;
	};

	Channel2op.prototype._keyOn = function() {
		this.op1._keyOn();
		this.op2._keyOn();
		this.feedback[0] = 0;
		this.feedback[1] = 0;
	};

	Channel2op.prototype._keyOff = function() {
		this.op1._keyOff();
		this.op2._keyOff();
	};

	Channel2op.prototype._updateOperators = function() {
		var keyScaleNumber = this.block * 2 + ((this.fnumh >> this.opl3Chip.nts) & 0x01);
		var f_number = (this.fnumh << 8) | this.fnuml;
		this.op1._updateOperator(keyScaleNumber, f_number, this.block);
		this.op2._updateOperator(keyScaleNumber, f_number, this.block);
	};

	Channel2op.prototype.toString = function() {
		var f_number = (this.fnumh << 8) + this.fnuml;
		var str = '';
		str += 'channelBaseAddress: ' + this.channelBaseAddress + '\n';
		str += 'f_number: ' + f_number + ', block: ' + this.block + '\n';
		str += 'cnt: ' + this.cnt + ', feedback: ' + this.feedback + '\n';
		str += 'op1:\n' + this.op1.toString();
		str += 'op2:\n' + this.op2.toString();
		return str;
	};

    return Channel2op;
})(Channel);







var Channel4op;

Channel4op = (function(_super) {

    OPL3JSUtil.__extends(Channel4op, _super);

    function Channel4op(opl3Chip, baseAddress, o1, o2, o3, o4) {
        this.op1 = o1;
        this.op2 = o2;
        this.op3 = o3;
        this.op4 = o4;
        return Channel4op.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
    }

    Channel4op.prototype.getChannelOutput = function() {
        var channelOutput = 0,
            op1Output = 0, op2Output = 0, op3Output = 0, op4Output = 0; // double

        var output; // double[]

        var secondChannelBaseAddress = this.channelBaseAddress + 3; // int
        var secondCnt = this.opl3Chip.registers[secondChannelBaseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset] & 0x1; // int
        var cnt4op = (this.cnt << 1) | secondCnt; // int

        var feedbackOutput = (this.feedback[0] + this.feedback[1]) / 2;

        switch (cnt4op) {
            case 0:
                if (this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);
                op2Output = this.op2.getOperatorOutput(op1Output * Channel.toPhase);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);
                channelOutput = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                break;
            case 1:
                if (this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);
                op2Output = this.op2.getOperatorOutput(op1Output * Channel.toPhase);

                op3Output = this.op3.getOperatorOutput(Operator.noModulator);
                op4Output = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                channelOutput = (op2Output + op4Output) / 2;
                break;
            case 2:
                if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);

                op2Output = this.op2.getOperatorOutput(Operator.noModulator);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);
                op4Output = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                channelOutput = (op1Output + op4Output) / 2;
                break;
            case 3:
                if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op3.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);

                op2Output = this.op2.getOperatorOutput(Operator.noModulator);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);

                op4Output = this.op4.getOperatorOutput(Operator.noModulator);

                channelOutput = (op1Output + op3Output + op4Output) / 3;
        }

        this.feedback[0] = this.feedback[1];
        this.feedback[1] = (op1Output * ChannelData.feedback[this.fb]) % 1;

        output = this._getInFourChannels(channelOutput);
        return output;
    };

    Channel4op.prototype._keyOn = function() {
        this.op1._keyOn();
        this.op2._keyOn();
        this.op3._keyOn();
        this.op4._keyOn();
        this.feedback[0] = this.feedback[1] = 0;
    };

    Channel4op.prototype._keyOff = function() {
        this.op1._keyOff();
        this.op2._keyOff();
        this.op3._keyOff();
        this.op4._keyOff();
    };

    Channel4op.prototype._updateOperators = function() {
        // Key Scale Number, used in EnvelopeGenerator.setActualRates().
        var keyScaleNumber = this.block * 2 + ((this.fnumh >> this.opl3Chip.nts) & 0x01);
        var f_number = (this.fnumh << 8) | this.fnuml;
        this.op1._updateOperator(keyScaleNumber, f_number, this.block);
        this.op2._updateOperator(keyScaleNumber, f_number, this.block);
        this.op3._updateOperator(keyScaleNumber, f_number, this.block);
        this.op4._updateOperator(keyScaleNumber, f_number, this.block);
    };

    Channel4op.prototype.toString = function() {
        var str = '';
        var f_number = (this.fnumh << 8) + this.fnuml;
        str += 'channelBaseAddress: ' + this.channelBaseAddress +'\n';
        str += 'f_number: ' + f_number + ', block: ' + this.block + '\n';
        str += 'cnt: ' + this.cnt + ', feedback: ' + this.fb + '\n';
        str += 'op1:\n' + this.op1.toString();
        str += 'op2:\n' + this.op2.toString();
        str += 'op3:\n' + this.op3.toString();
        str += 'op4:\n' + this.op4.toString();
        return str;
    };

    return Channel4op;
})(Channel);




var BassDrumChannel;

BassDrumChannel = (function(_super) {

    OPL3JSUtil.__extends(BassDrumChannel, _super);

	function BassDrumChannel(opl3Chip) {
		return BassDrumChannel.__super__.constructor.apply(this, [
            opl3Chip,
            BassDrumChannel.bassDrumChannelBaseAddress,
			new Operator(opl3Chip, BassDrumChannel.op1BaseAddress),
			new Operator(opl3Chip, BassDrumChannel.op2BaseAddress)
        ]);
	}

	BassDrumChannel.bassDrumChannelBaseAddress = 6;
	BassDrumChannel.op1BaseAddress = 0x10;
	BassDrumChannel.op2BaseAddress = 0x13;

	BassDrumChannel.prototype.getChannelOutput = function() {
		// Bass Drum ignores first operator, when it is in series.
		if (this.cnt == 1) {
			this.op1.ar = 0;
		}
		return BassDrumChannel.__super__.getChannelOutput.call(this);
	};

	// Key ON and OFF are unused in rhythm channels.
	BassDrumChannel.prototype._keyOn = function() {};
	BassDrumChannel.prototype._keyOff = function() {};

    return BassDrumChannel;
})(Channel2op);






var RhythmChannel;

RhythmChannel = (function(_super) {

    OPL3JSUtil.__extends(RhythmChannel, _super);

	function RhythmChannel(opl3Chip, baseAddress, o1, o2) {
		return RhythmChannel.__super__.constructor.apply(this, [
            opl3Chip,
            baseAddress,
            o1,
            o2
        ]);
	}

    RhythmChannel.prototype.getChannelOutput = function() {
		var channelOutput = 0, op1Output = 0, op2Output = 0; // double
		var output; // double[]

		// Note that, different from the common channel,
		// we do not check to see if the Operator's envelopes are Off.
		// Instead, we always do the calculations,
		// to update the publicly available phase.
		op1Output = this.op1.getOperatorOutput(Operator.noModulator);
		op2Output = this.op2.getOperatorOutput(Operator.noModulator);
		channelOutput = (op1Output + op2Output) / 2;

		output = this._getInFourChannels(channelOutput);
		return output;
	};

	// Rhythm channels are always running,
	// only the envelope is activated by the user.
    RhythmChannel.prototype._keyOn = function() {};
    RhythmChannel.prototype._keyOff = function() {};

    return RhythmChannel;
})(Channel2op);



var HighHatSnareDrumChannel;

HighHatSnareDrumChannel = (function(_super) {

    OPL3JSUtil.__extends(HighHatSnareDrumChannel, _super);

	function HighHatSnareDrumChannel(opl3Chip) {
		return HighHatSnareDrumChannel.__super__.constructor.apply(this, [
            opl3Chip,
            HighHatSnareDrumChannel.highHatSnareDrumChannelBaseAddress,
            opl3Chip.highHatOperator,
            opl3Chip.snareDrumOperator
        ]);
	}

	HighHatSnareDrumChannel.highHatSnareDrumChannelBaseAddress = 7;

    return HighHatSnareDrumChannel;
})(RhythmChannel);





var TomTomTopCymbalChannel;

TomTomTopCymbalChannel = (function(_super) {

    OPL3JSUtil.__extends(TomTomTopCymbalChannel, _super);

	function TomTomTopCymbalChannel(opl3Chip) {
		return TomTomTopCymbalChannel.__super__.constructor.apply(this, [
            opl3Chip,
            TomTomTopCymbalChannel.tomTomTopCymbalChannelBaseAddress,
            opl3Chip.tomTomOperator,
            opl3Chip.topCymbalOperator
        ]);
	}

	TomTomTopCymbalChannel.tomTomTopCymbalChannelBaseAddress = 8;

    return TomTomTopCymbalChannel;
})(RhythmChannel);



var DisabledChannel;

DisabledChannel = (function(_super) {

    OPL3JSUtil.__extends(DisabledChannel, _super);

    function DisabledChannel(opl3Chip) {
        return DisabledChannel.__super__.constructor.apply(this, [opl3Chip, 0]);
    }

    DisabledChannel.prototype.getChannelOutput = function() {
        return this.getInFourChannels(0);
    };

    DisabledChannel.prototype._keyOn = function() { };
    DisabledChannel.prototype._keyOff = function() { };
    DisabledChannel.prototype._updateOperators = function() { };

    return DisabledChannel;
})(Channel);


var OPL3;

OPL3 = (function() {
    function OPL3() {
        // Constructor stuff.
        this.registers = OPL3JSUtil.initArray(0x200, 0); // int[]

        this.operators = []; // Operator[][]
        this.channels2op = []; // Channel2op[][]
        this.channels4op = []; //Channel4op[][]
        this.channels = []; // Channel[][]
        this.disabledChannel = null; // DisabledChannel

        this.bassDrumChannel = null; // BassDrumChannel
        this.highHatSnareDrumChannel = null; // HighHatSnareDrumChannel
        this.tomTomTopCymbalChannel = null; // TomTomTopCymbalChannel
        this.highHatOperator = null; // HighHatOperator
        this.snareDrumOperator = null; // SnareDrumOperator
        this.tomTomOperator = null; // TomTomOperator
        this.topCymbalOperator = null; // TopCymbalOperator
        this.highHatOperatorInNonRhythmMode = null; // Operator
        this.snareDrumOperatorInNonRhythmMode = null; // Operator
        this.tomTomOperatorInNonRhythmMode = null; // Operator
        this.topCymbalOperatorInNonRhythmMode = null; // Operator
        this.nts = this.dam = this.dvb = this.ryt = this.bd = this.sd = this.tom = this.tc = this.hh = this._new = this.connectionsel = 0; // int
        this.vibratoIndex = this.tremoloIndex = 0; // int
        this.channels = [OPL3JSUtil.initArray(9, null), OPL3JSUtil.initArray(9, null)];
        this.output = OPL3JSUtil.initArray(4, 0); // short[]
        this.outputBuffer = OPL3JSUtil.initArray(4, 0.0); // double[]
        this._initOperators();
        this._initChannels2op();
        this._initChannels4op();
        this._initRhythmChannels();
        this._initChannels();
    }

    // ########
    // Static properties
    // ########

    OPL3.OutputFormat = {
        INT16 : 'INT16',
        FLOAT32 : 'FLOAT32'
    };

    // ########
    // Public methods
    // ########
    // The methods read() and write() are the only
    // ones needed by the user to interface with the emulator.
    // read() returns one frame at a time, to be played at 49700 Hz,
    // with each frame being four 16-bit samples (or 32-bit floats),
    // corresponding to the OPL3 four output channels CHA...CHD.
    OPL3.prototype.read = function(outputFormat) {
        if (outputFormat === undefined) {
            outputFormat = OPL3.OutputFormat.INT16;
        }

        var output = this.output;
        OPL3JSUtil.fillArray(output, 0); // short[]
        var outputBuffer = this.outputBuffer;
        OPL3JSUtil.fillArray(outputBuffer, 0.0); // double[]
        var channelOutput = null; // double[]

        // If _new = 0, use OPL2 mode with 9 channels. If _new = 1, use OPL3 18 channels;
        for (var array = 0; array < (this._new + 1); array++) {
            for (var channelNumber = 0; channelNumber < 9; channelNumber++) {
                // Reads output from each OPL3 channel, and accumulates it in the output buffer:
                channelOutput = this.channels[array][channelNumber].getChannelOutput();
                for (var outputChannelNumber2 = 0; outputChannelNumber2 < 4; outputChannelNumber2++) {
                    outputBuffer[outputChannelNumber2] += channelOutput[outputChannelNumber2];
                }
            }
        }

        // Normalizes the output buffer after all channels have been added,
        // with a maximum of 18 channels,
        // and multiplies it to get the 16 bit signed output.
        for (var outputChannelNumber3 = 0; outputChannelNumber3 < 4; outputChannelNumber3++) {
            var tmpValue = outputBuffer[outputChannelNumber3] / 18.0;
            if (outputFormat === OPL3.OutputFormat.FLOAT32) {
                // do nothing
            } else {
                tmpValue = parseInt(tmpValue * 0x7FFF);
            }
            output[outputChannelNumber3] = tmpValue;
        }

        // Advances the OPL3-wide vibrato index, which is used by
        // PhaseGenerator.getPhase() in each Operator.
        this.vibratoIndex++;
        if (this.vibratoIndex >= OPL3Data.vibratoTable[this.dvb].length) {
            this.vibratoIndex = 0;
        }
        // Advances the OPL3-wide tremolo index, which is used by
        // EnvelopeGenerator.getEnvelope() in each Operator.
        this.tremoloIndex++;
        if (this.tremoloIndex >= OPL3Data.tremoloTable[this.dam].length) {
            this.tremoloIndex = 0;
        }

        return output;
    };

    OPL3.prototype.write = function(array, address, data) {
        // The OPL3 has two registers arrays, each with adresses ranging
        // from 0x00 to 0xF5.
        // This emulator uses one array, with the two original register arrays
        // starting at 0x00 and at 0x100.
        var registerAddress = (array << 8) | address; // int
        // If the address is out of the OPL3 memory map, returns.
        if (registerAddress < 0 || registerAddress >= 0x200) return;

        this.registers[registerAddress] = data;
        switch (address & 0xE0) {
            // The first 3 bits masking gives the type of the register by using its base address:
            // 0x00, 0x20, 0x40, 0x60, 0x80, 0xA0, 0xC0, 0xE0
            // When it is needed, we further separate the register type inside each base address,
            // which is the case of 0x00 and 0xA0.

            // Through out this emulator we will use the same name convention to
            // reference a byte with several bit registers.
            // The name of each bit register will be followed by the number of bits
            // it occupies inside the byte.
            // Numbers without accompanying names are unused bits.
            case 0x00:
                // Unique registers for the entire OPL3:
                if (array == 1) {
                    if (address == 0x04) {
                        this._update_2_CONNECTIONSEL6();
                    } else if (address == 0x05) {
                        this._update_7_NEW1();
                    }
                }
                else if (address == 0x08) {
                    this._update_1_NTS1_6();
                }
                break;

            case 0xA0:
                // 0xBD is a control register for the entire OPL3:
                if (address == 0xBD) {
                    if (array == 0) {
                        this._update_DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1();
                    }
                    break;
                }
                // Registers for each channel are in A0-A8, B0-B8, C0-C8, in both register arrays.
                // 0xB0...0xB8 keeps kon,block,fnum(h) for each channel.
                if ((address & 0xF0) == 0xB0 && address <= 0xB8) {
                    // If the address is in the second register array, adds 9 to the channel number.
                    // The channel number is given by the last four bits, like in A0,...,A8.
                    this.channels[array][address & 0x0F].update_2_KON1_BLOCK3_FNUMH2();
                    break;
                }
                // 0xA0...0xA8 keeps fnum(l) for each channel.
                if ((address & 0xF0) == 0xA0 && address <= 0xA8)
                    this.channels[array][address & 0x0F].update_FNUML8();
                break;
            // 0xC0...0xC8 keeps cha,chb,chc,chd,fb,cnt for each channel:
            case 0xC0:
                if (address <= 0xC8)
                    this.channels[array][address & 0x0F].update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
                break;

            // Registers for each of the 36 Operators:
            default:
                var operatorOffset = address & 0x1F;
                if (this.operators[array][operatorOffset] == null) {
                    break;
                }
                switch (address & 0xE0) {
                    // 0x20...0x35 keeps am,vib,egt,ksr,mult for each operator:
                    case 0x20:
                        this.operators[array][operatorOffset].update_AM1_VIB1_EGT1_KSR1_MULT4();
                        break;
                    // 0x40...0x55 keeps ksl,tl for each operator:
                    case 0x40:
                        this.operators[array][operatorOffset].update_KSL2_TL6();
                        break;
                    // 0x60...0x75 keeps ar,dr for each operator:
                    case 0x60:
                        this.operators[array][operatorOffset].update_AR4_DR4();
                        break;
                    // 0x80...0x95 keeps sl,rr for each operator:
                    case 0x80:
                        this.operators[array][operatorOffset].update_SL4_RR4();
                        break;
                    // 0xE0...0xF5 keeps ws for each operator:
                    case 0xE0:
                        this.operators[array][operatorOffset].update_5_WS3();
                }
        }
    };

    // ########
    // Private methods
    // ########
    OPL3.prototype._initOperators = function() {
        var baseAddress;
        // The YMF262 has 36 operators:
        this.operators = [
            OPL3JSUtil.initArray(0x20, null),
            OPL3JSUtil.initArray(0x20, null)
        ];
        for (var array = 0; array < 2; array++) {
            for (var group = 0; group <= 0x10; group += 8) {
                for (var offset = 0; offset < 6; offset++) {
                    baseAddress = (array << 8) | (group + offset);
                    this.operators[array][group + offset] = new Operator(this, baseAddress);
                }
            }
        }

        // Create specific operators to switch when in rhythm mode:
        this.highHatOperator = new HighHatOperator(this);
        this.snareDrumOperator = new SnareDrumOperator(this);
        this.tomTomOperator = new TomTomOperator(this);
        this.topCymbalOperator = new TopCymbalOperator(this);

        // Save operators when they are in non-rhythm mode:
        // Channel 7:
        this.highHatOperatorInNonRhythmMode = this.operators[0][0x11];
        this.snareDrumOperatorInNonRhythmMode = this.operators[0][0x14];
        // Channel 8:
        this.tomTomOperatorInNonRhythmMode = this.operators[0][0x12];
        this.topCymbalOperatorInNonRhythmMode = this.operators[0][0x15];
    };

    OPL3.prototype._initChannels2op = function() {
        // The YMF262 has 18 2-op channels.
        // Each 2-op channel can be at a serial or parallel operator configuration:
        this.channels2op = [
            OPL3JSUtil.initArray(9, null),
            OPL3JSUtil.initArray(9, null)
        ]; // Channel2op[2][9]

        for (var array = 0; array < 2; array++) {
            for (var channelNumber = 0; channelNumber < 3; channelNumber++) {
                var baseAddress = (array << 8) | channelNumber;
                // Channels 1, 2, 3 -> Operator offsets 0x0,0x3; 0x1,0x4; 0x2,0x5
                this.channels2op[array][channelNumber] = new Channel2op(this, baseAddress, this.operators[array][channelNumber], this.operators[array][channelNumber + 0x3]);
                // Channels 4, 5, 6 -> Operator offsets 0x8,0xB; 0x9,0xC; 0xA,0xD
                this.channels2op[array][channelNumber + 3] = new Channel2op(this, baseAddress + 3, this.operators[array][channelNumber + 0x8], this.operators[array][channelNumber + 0xB]);
                // Channels 7, 8, 9 -> Operators 0x10,0x13; 0x11,0x14; 0x12,0x15
                this.channels2op[array][channelNumber + 6] = new Channel2op(this, baseAddress + 6, this.operators[array][channelNumber + 0x10], this.operators[array][channelNumber + 0x13]);
            }
        }
    };

    OPL3.prototype._initChannels4op = function() {
        // The YMF262 has 3 4-op channels in each array:
        this.channels4op = [
            OPL3JSUtil.initArray(3, null),
            OPL3JSUtil.initArray(3, null)
        ]; //Channel4op[2][3];
        for (var array = 0; array < 2; array++) {
            for (var channelNumber = 0; channelNumber < 3; channelNumber++) {
                var baseAddress = (array << 8) | channelNumber;
                // Channels 1, 2, 3 -> Operators 0x0,0x3,0x8,0xB; 0x1,0x4,0x9,0xC; 0x2,0x5,0xA,0xD;
                this.channels4op[array][channelNumber] = new Channel4op(this, baseAddress, this.operators[array][channelNumber], this.operators[array][channelNumber + 0x3], this.operators[array][channelNumber + 0x8], this.operators[array][channelNumber + 0xB]);
            }
        }
    };

    OPL3.prototype._initRhythmChannels = function() {
        this.bassDrumChannel = new BassDrumChannel(this);
        this.highHatSnareDrumChannel = new HighHatSnareDrumChannel(this);
        this.tomTomTopCymbalChannel = new TomTomTopCymbalChannel(this);
    };

    OPL3.prototype._initChannels = function() {
        // Channel is an abstract class that can be a 2-op, 4-op, rhythm or disabled channel,
        // depending on the OPL3 configuration at the time.
        // channels[] inits as a 2-op serial channel array:
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 9; i++) {
                this.channels[array][i] = this.channels2op[array][i];
            }
        }

        // Unique instance to fill future gaps in the Channel array,
        // when there will be switches between 2op and 4op mode.
        this.disabledChannel = new DisabledChannel(this);
    };

    OPL3.prototype._update_1_NTS1_6 = function() {
        var _1_nts1_6 = this.registers[OPL3Data._1_NTS1_6_Offset];
        // Note Selection. This register is used in Channel.updateOperators() implementations,
        // to calculate the channel´s Key Scale Number.
        // The value of the actual envelope rate follows the value of
        // OPL3.nts,Operator.keyScaleNumber and Operator.ksr
        this.nts = (_1_nts1_6 & 0x40) >> 6;
    };

    OPL3.prototype._update_DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1 = function() {
        var dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 = this.registers[OPL3Data.DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1_Offset];
        // Depth of amplitude. This register is used in EnvelopeGenerator.getEnvelope();
        this.dam = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x80) >> 7;

        // Depth of vibrato. This register is used in PhaseGenerator.getPhase();
        this.dvb = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x40) >> 6;

        var new_ryt = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x20) >> 5;
        if (new_ryt != this.ryt) {
            this.ryt = new_ryt;
            this._setRhythmMode();
        }

        var new_bd = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x10) >> 4;
        if (new_bd != this.bd) {
            this.bd = new_bd;
            if (this.bd == 1) {
                this.bassDrumChannel.op1._keyOn();
                this.bassDrumChannel.op2._keyOn();
            }
        }

        var new_sd = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x08) >> 3;
        if (new_sd != this.sd) {
            this.sd = new_sd;
            if (this.sd == 1) {
                this.snareDrumOperator._keyOn();
            }
        }

        var new_tom = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x04) >> 2;
        if (new_tom != this.tom) {
            this.tom = new_tom;
            if (this.tom == 1)  {
                this.tomTomOperator._keyOn();
            }
        }

        var new_tc = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x02) >> 1;
        if (new_tc != this.tc) {
            this.tc = new_tc;
            if (this.tc == 1) {
                this.topCymbalOperator._keyOn();
            }
        }

        var new_hh = dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x01;
        if (new_hh != this.hh) {
            this.hh = new_hh;
            if (this.hh == 1) {
                this.highHatOperator._keyOn();
            }
        }
    };

    OPL3.prototype._update_7_NEW1 = function() {
        var _7_new1 = this.registers[OPL3Data._7_NEW1_Offset];
        // OPL2/OPL3 mode selection. This register is used in
        // OPL3.read(), OPL3.write() and Operator.getOperatorOutput();
        this._new = (_7_new1 & 0x01);
        if (this._new == 1) {
            this._setEnabledChannels();
        }
        this._set4opConnections();
    };

    OPL3.prototype._setEnabledChannels = function() {
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 9; i++) {
                var baseAddress = this.channels[array][i].channelBaseAddress;
                this.registers[baseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset] |= 0xF0;
                this.channels[array][i].update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
            }
        }
    };

    OPL3.prototype._update_2_CONNECTIONSEL6 = function() {
        // This method is called only if _new is set.
        var _2_connectionsel6 = this.registers[OPL3Data._2_CONNECTIONSEL6_Offset];
        // 2-op/4-op channel selection. This register is used here to configure the OPL3.channels[] array.
        this.connectionsel = (_2_connectionsel6 & 0x3F);
        this._set4opConnections();
    };

    OPL3.prototype._set4opConnections = function() {
        // bits 0, 1, 2 sets respectively 2-op channels (1,4), (2,5), (3,6) to 4-op operation.
        // bits 3, 4, 5 sets respectively 2-op channels (10,13), (11,14), (12,15) to 4-op operation.
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 3; i++) {
                if (this._new == 1) {
                    var shift = array * 3 + i;
                    var connectionBit = (this.connectionsel >> shift) & 0x01;
                    if (connectionBit == 1) {
                        this.channels[array][i] = this.channels4op[array][i];
                        this.channels[array][i + 3] = this.disabledChannel;
                        this.channels[array][i].updateChannel();
                        continue;
                    }
                }
                this.channels[array][i] = this.channels2op[array][i];
                this.channels[array][i + 3] = this.channels2op[array][i + 3];
                this.channels[array][i].updateChannel();
                this.channels[array][i + 3].updateChannel();
            }
        }
    };

    OPL3.prototype._setRhythmMode = function() {
        var i;
        if (this.ryt == 1) {
            this.channels[0][6] = this.bassDrumChannel;
            this.channels[0][7] = this.highHatSnareDrumChannel;
            this.channels[0][8] = this.tomTomTopCymbalChannel;
            this.operators[0][0x11] = this.highHatOperator;
            this.operators[0][0x14] = this.snareDrumOperator;
            this.operators[0][0x12] = this.tomTomOperator;
            this.operators[0][0x15] = this.topCymbalOperator;
        }
        else {
            for (i = 6; i <= 8; i++) {
                this.channels[0][i] = this.channels2op[0][i];
            }
            this.operators[0][0x11] = this.highHatOperatorInNonRhythmMode;
            this.operators[0][0x14] = this.snareDrumOperatorInNonRhythmMode;
            this.operators[0][0x12] = this.tomTomOperatorInNonRhythmMode;
            this.operators[0][0x15] = this.topCymbalOperatorInNonRhythmMode;
        }
        for(i = 6; i <= 8; i++) {
            this.channels[0][i].updateChannel();
        }
    };

    return OPL3;
})();



var DRODecoder = AV.Decoder.extend(function() {
    AV.Decoder.register('dro2', this);

    // Aurora methods
    this.prototype.init = function() {
        this.opl = new OPL3();
        this.headerData = null;
        this.delay = 0;
        this.bank = 0;
        this.sampleRate = 49700;
        this.channels = 2; // hm
        this.bps = 16;
        this.bufferTime = 250; // in ms
        this.maxBufferSize = this.__msToSamples(this.bufferTime);
        this.bufferIndex = 0;

        // Amp & clamp is derived from logic in PyOPL
        this.amplification = 1;
        var sampleSize = this.bps / 8;
        var sampBits = sampleSize << 3;
        this.SAMP_MAX = (1 << (sampBits - 1)) - 1;
        this.SAMP_MIN = -(1 << (sampBits - 1));
    };

    this.prototype.setCookie = function(headerData) {
        this.headerData = headerData;
    };

    this.prototype.readChunk = function() {
        // Note quite right - sample rate must match the OPL3 sample rate, but
        // this results in dropped samples due to rounding errors.
        var buffer = new Int16Array(this.maxBufferSize);
        this.bufferIndex = 0;
        var timeRendered = 0;

        // To prevent corrupting the state of the emulated OPL3 chip,
        //  we first collect all instructions & data that we need from the
        //  stream, and *then* write the instructions to the chip and perform rendering.
        // This should prevent underflows during processing.
        var instructions = [];
        if (this.delay < this.bufferTime) {
            var chunkTime = this.bufferTime - this.delay;
            instructions = this.__collectInstructionsForChunk(chunkTime);
        }

        // Render any leftover time from last instructions processed
        if (this.delay > 0) {
            timeRendered += this.__renderDelay(buffer, timeRendered);
        }

        // Process instructions and render
        for (var i = 0; i < instructions.length; i++) {
            var instructionsBlock = instructions[i];
            this.__writeInstructions(instructionsBlock.instructions);
            this.delay = instructionsBlock.delay;
            timeRendered += this.__renderDelay(buffer, timeRendered);
        }

        return buffer;
    };

    // Internal methods
    this.prototype.__renderDelay = function(buffer, timeRendered) {
        var timeToRender = Math.min(this.bufferTime - timeRendered, this.delay);
        this.__render(buffer, timeToRender);
        this.delay -= timeToRender;
        return timeToRender;
    };

    this.prototype.__collectInstructionsForChunk = function(chunkTime) {
        var instructionsAndDelays = [];
        var totalDelay = 0;
        while (totalDelay < chunkTime) {
            var instructionsBlock = this.__processInstructions();
            instructionsAndDelays.push(instructionsBlock);
            totalDelay += instructionsBlock.delay;
        }
        return instructionsAndDelays;
    };

    this.prototype.__processInstructions = function() {
        var instructionsBlock = {
            delay: 0,
            instructions: []
        };
        while (instructionsBlock.delay == 0) {
            this.__processInstruction(instructionsBlock);
        }
        return instructionsBlock;
    };

    this.prototype.__processInstruction = function(instructionsBlock) {
        var datum = this.stream.readUInt8();
        switch (datum) {
            case this.headerData.shortDelayCode:
                instructionsBlock.delay = this.stream.readUInt8() + 1;
                break;
            case this.headerData.longDelayCode:
                instructionsBlock.delay = (this.stream.readUInt8() + 1) << 8;
                break;
            default:
                this.bank = datum & 0x80;
                var register_code = datum & 0x7F;
                var register = this.headerData.codemap[register_code];
                var value = this.stream.readUInt8();
                instructionsBlock.instructions.push({
                    bank: this.bank,
                    register: register,
                    value: value
                });
                break;
        }
    };

    this.prototype.__writeInstructions = function(instructionsToWrite) {
        for (var i = 0; i < instructionsToWrite.length; i++) {
            var instruction = instructionsToWrite[i];
            this.opl.write(instruction.bank, instruction.register, instruction.value);
        }
    };

    this.prototype.__render = function(buffer, timeToRender) {
        var samplesToRender = this.__msToSamples(timeToRender);
        for (var i = 0; i < samplesToRender; i += 2) {
            var samples = this.opl.read(OPL3.OutputFormat.INT16);
            buffer[this.bufferIndex++] = this.__amplifyAndClamp(samples[0]);
            buffer[this.bufferIndex++] = this.__amplifyAndClamp(samples[1]);
            // What are the other two output channels for? Stereo and/or dual-chips?
        }
    };

    this.prototype.__msToSamples = function(ms) {
        // This isn't useful when dealing with fractions - it only
        //  seems to work when sample rate is 48000.
        return Math.floor(ms * this.sampleRate * this.channels / 1000);
    };

    this.prototype.__amplifyAndClamp = function(sample) {
        return Math.min(this.SAMP_MAX, Math.max(this.SAMP_MIN, sample << this.amplification));
    };
});


