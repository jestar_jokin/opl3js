importScripts('aurora_slim.js', 'output.js');

var player = null;
var openFile = null;
var asset = null;

// HACK for Firefox, which does not expose FileReader in worker threads.
try {
    FileReader;
} catch (err) {
    function dummyFunc() {}

    function FileReaderAdapter() {
        this._fileReaderSync = new FileReaderSync();
    }

    FileReaderAdapter.prototype.onerror = dummyFunc;
    FileReaderAdapter.prototype.onload = dummyFunc;
    FileReaderAdapter.prototype.onloadend = dummyFunc;
    FileReaderAdapter.prototype.onprogress = dummyFunc;


    FileReaderAdapter.prototype.readAsArrayBuffer = function(blob) {
        try {
            console.log("Reading");
            var buffer = this._fileReaderSync.readAsArrayBuffer(blob);
            console.log("Finished reading");
            this.onload({
                target: {
                    result: buffer
                }
            });
            this.onloadend();
            console.log("Totally finished");
        } catch (err) {
            console.log("Errored");
            this.onerror(err);
        }
    };

    FileReader = FileReaderAdapter;
}

/**
 * @class
 */
function WorkerMessage(action, args) {
    this.action = action;
    this.args = args;
}

function sendMessage(key, args) {
    postMessage(new WorkerMessage(key, args));
}

function startDecoding(file) {
    var asset = AV.Asset.fromFile(file);

    asset.on('error', function (err) {
        postMessage(new WorkerMessage('error', [err]));
    });

    asset.decodeToBuffer(function (buffer) {
        console.log("Decoded");
        postMessage(new WorkerMessage('audioBuffer', [buffer]));
    });
}

function wrapProxy(key) {
    return function () {
        sendMessage(key, Array.prototype.slice.call(arguments));
    }
}

var ACTION_MAP = {
    'Asset.fromFile': function (file) {
        asset = AV.Asset.fromFile(file);
        asset.on('error', wrapProxy('error'));
        asset.on('buffer', wrapProxy('Asset.buffer'));
        asset.on('duration', wrapProxy('Asset.duration'));
        asset.on('metadata', wrapProxy('Asset.metadata'));
        asset.on('format', wrapProxy('Asset.format'));
        asset.on('data', wrapProxy('Asset.data'));
        asset.on('end', wrapProxy('Asset.end'));
        asset.on('decodeStart', wrapProxy('Asset.decodeStart'));
        sendMessage('assetLoaded', []);
    },
    'Asset.start': function () {
        asset.start();
    },
    'Asset.decodePacket': function () {
        asset.decodePacket();
    },

    'Player.fromFile': function (file) {
        openFile = file;
        console.log(file);
        player = AV.Player.fromFile(file);
        player.play();
    },
    'Player.pause': function () {
        if (player) {
            player.pause();
        }
    },
    'Player.play': function () {
        if (player) {
            player.play();
        } else if (openFile) {
            player = AV.Player.fromFile(openFile);
            player.play();
        }
    },
    'Player.startDecoding': startDecoding,
    'Player.stop': function () {
        if (player) {
            player.stop();
            player.destroy();
            player = null;
        }
    }
};


function dispatchMessage(msg) {
    var data = msg.data;
    var action = data.action;
    console.log("Message received in worker: " + action);
    var args = data.args;
    var func = ACTION_MAP[action];
    if (func) {
        return func.apply(null, args);
    } else {
        console.log("Ignoring unknown action: " + action);
    }
}

onmessage = dispatchMessage;