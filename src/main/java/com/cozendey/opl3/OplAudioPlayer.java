package com.cozendey.opl3;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class OplAudioPlayer {
    final static int sampleRate = 49700;
    OPL3 opl;
    SourceDataLine sourceDataLine;
    int entireMusicLength;

    public OplAudioPlayer() {
        this.opl = new OPL3();
    }

    protected byte[] shortsToBytes(short[] inputData) {
        byte[] outBytes = new byte[inputData.length * 2];
        short value;
        for (int i = 0; i < inputData.length; i++) {
            value = inputData[i];
            outBytes[(i * 2)] = (byte)(value & 0xFF);
            outBytes[(i * 2) + 1] = (byte)((value & 0xFF00) >> 8);
        }
        return outBytes;
    }

    public void writeData(short[][] inputData) {
        for (short[] inputDatum : inputData) {
            this.opl.write(0, inputDatum[0], inputDatum[1]);
        }
    }

    public void renderAudio(double secondsToRender) {
        int samplesToRender = (int)(OplAudioPlayer.sampleRate * secondsToRender);
        int numBytes = samplesToRender * 4; // 2 channels, 16-bit values as bytes
        byte[] musicBuffer = new byte[numBytes];

        this.entireMusicLength = numBytes;
        startSourceDataLine();
        short[] temp;
        short left;
        short right;
        for (int i = 0; i < numBytes; i += 4) {
            temp = this.opl.read();
            left = temp[0];
            right = temp[1];
            musicBuffer[i] = (byte)(left & 0xFF);
            musicBuffer[i + 1] = (byte)((left & 0xFF00) >> 8);
            musicBuffer[i + 2] = (byte)(right & 0xFF);
            musicBuffer[i + 3] = (byte)((right & 0xFF00) >> 8);
        }
        this.sourceDataLine.write(musicBuffer, 0, numBytes);
        stopSourceDataLine();
    }

    public void startSourceDataLine()
    {
        AudioFormat localAudioFormat = new AudioFormat(OplAudioPlayer.sampleRate, 16, 2, true, false);
        try
        {
            this.sourceDataLine = AudioSystem.getSourceDataLine(localAudioFormat);
            this.sourceDataLine.open(localAudioFormat, this.entireMusicLength);
        }
        catch (LineUnavailableException lue)
        {
            throw new RuntimeException(lue);
        }
        this.sourceDataLine.start();
    }

    public void stopSourceDataLine()
    {
        this.sourceDataLine.drain();
        this.sourceDataLine.stop();
        this.sourceDataLine.close();
    }
}
