//import "../emulator/OPL3.js"

var DRODecoder = AV.Decoder.extend(function() {
    AV.Decoder.register('dro2', this);

    // Aurora methods
    this.prototype.init = function() {
        this.opl = new OPL3();
        this.headerData = null;
        this.delay = 0;
        this.bank = 0;
        this.sampleRate = 49700;
        this.channels = 2; // hm
        this.bps = 16;
        this.bufferTime = 250; // in ms
        this.maxBufferSize = this.__msToSamples(this.bufferTime);
        this.bufferIndex = 0;

        // Amp & clamp is derived from logic in PyOPL
        this.amplification = 1;
        var sampleSize = this.bps / 8;
        var sampBits = sampleSize << 3;
        this.SAMP_MAX = (1 << (sampBits - 1)) - 1;
        this.SAMP_MIN = -(1 << (sampBits - 1));
    };

    this.prototype.setCookie = function(headerData) {
        this.headerData = headerData;
    };

    this.prototype.readChunk = function() {
        // Note quite right - sample rate must match the OPL3 sample rate, but
        // this results in dropped samples due to rounding errors.
        var buffer = new Int16Array(this.maxBufferSize);
        this.bufferIndex = 0;
        var timeRendered = 0;

        // To prevent corrupting the state of the emulated OPL3 chip,
        //  we first collect all instructions & data that we need from the
        //  stream, and *then* write the instructions to the chip and perform rendering.
        // This should prevent underflows during processing.
        var instructions = [];
        if (this.delay < this.bufferTime) {
            var chunkTime = this.bufferTime - this.delay;
            instructions = this.__collectInstructionsForChunk(chunkTime);
        }

        // Render any leftover time from last instructions processed
        if (this.delay > 0) {
            timeRendered += this.__renderDelay(buffer, timeRendered);
        }

        // Process instructions and render
        for (var i = 0; i < instructions.length; i++) {
            var instructionsBlock = instructions[i];
            this.__writeInstructions(instructionsBlock.instructions);
            this.delay = instructionsBlock.delay;
            timeRendered += this.__renderDelay(buffer, timeRendered);
        }

        return buffer;
    };

    // Internal methods
    this.prototype.__renderDelay = function(buffer, timeRendered) {
        var timeToRender = Math.min(this.bufferTime - timeRendered, this.delay);
        this.__render(buffer, timeToRender);
        this.delay -= timeToRender;
        return timeToRender;
    };

    this.prototype.__collectInstructionsForChunk = function(chunkTime) {
        var instructionsAndDelays = [];
        var totalDelay = 0;
        while (totalDelay < chunkTime) {
            var instructionsBlock = this.__processInstructions();
            instructionsAndDelays.push(instructionsBlock);
            totalDelay += instructionsBlock.delay;
        }
        return instructionsAndDelays;
    };

    this.prototype.__processInstructions = function() {
        var instructionsBlock = {
            delay: 0,
            instructions: []
        };
        while (instructionsBlock.delay == 0) {
            this.__processInstruction(instructionsBlock);
        }
        return instructionsBlock;
    };

    this.prototype.__processInstruction = function(instructionsBlock) {
        var datum = this.stream.readUInt8();
        switch (datum) {
            case this.headerData.shortDelayCode:
                instructionsBlock.delay = this.stream.readUInt8() + 1;
                break;
            case this.headerData.longDelayCode:
                instructionsBlock.delay = (this.stream.readUInt8() + 1) << 8;
                break;
            default:
                this.bank = datum & 0x80;
                var register_code = datum & 0x7F;
                var register = this.headerData.codemap[register_code];
                var value = this.stream.readUInt8();
                instructionsBlock.instructions.push({
                    bank: this.bank,
                    register: register,
                    value: value
                });
                break;
        }
    };

    this.prototype.__writeInstructions = function(instructionsToWrite) {
        for (var i = 0; i < instructionsToWrite.length; i++) {
            var instruction = instructionsToWrite[i];
            this.opl.write(instruction.bank, instruction.register, instruction.value);
        }
    };

    this.prototype.__render = function(buffer, timeToRender) {
        var samplesToRender = this.__msToSamples(timeToRender);
        for (var i = 0; i < samplesToRender; i += 2) {
            var samples = this.opl.read(OPL3.OutputFormat.INT16);
            buffer[this.bufferIndex++] = this.__amplifyAndClamp(samples[0]);
            buffer[this.bufferIndex++] = this.__amplifyAndClamp(samples[1]);
            // What are the other two output channels for? Stereo and/or dual-chips?
        }
    };

    this.prototype.__msToSamples = function(ms) {
        // This isn't useful when dealing with fractions - it only
        //  seems to work when sample rate is 48000.
        return Math.floor(ms * this.sampleRate * this.channels / 1000);
    };

    this.prototype.__amplifyAndClamp = function(sample) {
        return Math.min(this.SAMP_MAX, Math.max(this.SAMP_MIN, sample << this.amplification));
    };
});

