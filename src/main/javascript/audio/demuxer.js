var DRODemuxer = AV.Demuxer.extend(function() {
    AV.Demuxer.register(this);

    // Aurora methods
    this.prototype.init = function() {
        this.headerData = null;
        this.headerLoaded = false;
    };


    this.probe = function(buffer) {
        return buffer.peekString(0, 8) == 'DBRAWOPL';
    };

    this.prototype.readChunk = function() {
        var stream = this.stream;

        if (!this.headerLoaded) {
            if (stream.offset == 0) {
                if (stream.available(26)) {
                    this.__readHeader();
                } else {
                    return null;
                }
            }
            if (stream.offset == 26) {
                if (stream.available(this.headerData.codemapLength)) {
                    this.emit('format', {
                        formatID: 'dro2',    // <= 4 letter string representing the decoder that should be used
                        sampleRate: 49700,   // the sampleRate of the file
                        channelsPerFrame: 2, // the number of audio channels
                        bitsPerChannel: 16   // the sample bit depth
                    });
                    this.emit('duration', this.headerData.lengthMs);
                    this.__readCodemap();
                } else {
                    return null;
                }
            }
        }

        if (stream.offset >= this.headerData.lengthPairs * 2 + 26 + this.headerData.codemapLength) {
            return this.emit('end');
        }

        while (stream.available(1)) {
            var buffer = stream.readSingleBuffer(stream.remainingBytes());
            this.emit('data', buffer);
        }
    };

    // Internal methods
    this.prototype._readUInt8 = function() {
        return this.stream.readUInt8();
    };

    this.prototype._readUInt16LE = function() {
        return this.stream.readUInt16(true);
    };

    this.prototype._readUInt32LE = function() {
        return this.stream.readUInt32(true);
    };

    this.prototype.__readHeader = function() {
        var headerData = {};
        var header = this.stream.readString(8);
        if (header != 'DBRAWOPL') {
            return this.emit('error', 'Incorrect DRO header, expected DBRAWOPL, found ' + header);
        }
        var ver1 = this._readUInt16LE();
        var ver2 = this._readUInt16LE();
        if (ver1 !== 2 && ver2 !== 0) { // only support DRO v2 for now
            return this.emit('error', 'Unsupported DRO version, expected 2.0, found ' + ver1 + '.' + ver2);
        }
        headerData.droVersion = 2;
        headerData.lengthPairs = this._readUInt32LE();
        headerData.lengthMs = this._readUInt32LE();
        headerData.hardwareType = this._readUInt8();
        var iFormat = this._readUInt8();
        if (iFormat != 0) {
            return this.emit('error', 'Unsupported DRO v2 format, only format 0 is supported; found ' + iFormat);
        }
        var iCompression = this._readUInt8();
        if (iCompression != 0) {
            return this.emit('error', 'Unsupported DRO v2 compression, only compression 0 is supported; found ' + iCompression);
        }
        headerData.shortDelayCode = this._readUInt8();
        headerData.longDelayCode = this._readUInt8();
        headerData.codemapLength = this._readUInt8();
        headerData.codemap = [];
        this.headerData = headerData;
    };

    this.prototype.__readCodemap = function() {
        for (var i = 0; i < this.headerData.codemapLength; i++) {
            this.headerData.codemap[i] = this.stream.readUInt8();
        }
        this.emit('cookie', this.headerData);
        this.headerLoaded = true;
    };
});