//import "OPL3JSUtil.js"
//import "EnvelopeGenerator.js"
//import "Channel.js"
//import "Operator.js"
//import "ChannelData.js"

var Channel2op;

Channel2op = (function(_super) {

	OPL3JSUtil.__extends(Channel2op, _super);

	function Channel2op(opl3Chip, baseAddress, o1, o2) {
		this.op1 = o1;
		this.op2 = o2;
		return Channel2op.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
	}

	Channel2op.prototype.getChannelOutput = function() {
		var channelOutput = 0; // double
		var op1Output = 0; // double
		var op2Output = 0; // double
		var output; // double[]
		// The feedback uses the last two outputs from
		// the first operator, instead of just the last one.
		var feedbackOutput = (this.feedback[0] + this.feedback[1]) / 2; // double

		switch (this.cnt) {
			// CNT = 0, the operators are in series, with the first in feedback.
			case 0:
				if (this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
					return this._getInFourChannels(0);
				op1Output = this.op1.getOperatorOutput(feedbackOutput);
				channelOutput = this.op2.getOperatorOutput(op1Output * Channel.toPhase);
				break;
			// CNT = 1, the operators are in parallel, with the first in feedback.
			case 1:
				if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
					this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
					return this._getInFourChannels(0);
				op1Output = this.op1.getOperatorOutput(feedbackOutput);
				op2Output = this.op2.getOperatorOutput(Operator.noModulator);
				channelOutput = (op1Output + op2Output) / 2;
				break;
		}

		this.feedback[0] = this.feedback[1];
		this.feedback[1] = (op1Output * ChannelData.feedback[this.fb]) % 1;
		output = this._getInFourChannels(channelOutput);
		return output;
	};

	Channel2op.prototype._keyOn = function() {
		this.op1._keyOn();
		this.op2._keyOn();
		this.feedback[0] = 0;
		this.feedback[1] = 0;
	};

	Channel2op.prototype._keyOff = function() {
		this.op1._keyOff();
		this.op2._keyOff();
	};

	Channel2op.prototype._updateOperators = function() {
		var keyScaleNumber = this.block * 2 + ((this.fnumh >> this.opl3Chip.nts) & 0x01);
		var f_number = (this.fnumh << 8) | this.fnuml;
		this.op1._updateOperator(keyScaleNumber, f_number, this.block);
		this.op2._updateOperator(keyScaleNumber, f_number, this.block);
	};

	Channel2op.prototype.toString = function() {
		var f_number = (this.fnumh << 8) + this.fnuml;
		var str = '';
		str += 'channelBaseAddress: ' + this.channelBaseAddress + '\n';
		str += 'f_number: ' + f_number + ', block: ' + this.block + '\n';
		str += 'cnt: ' + this.cnt + ', feedback: ' + this.feedback + '\n';
		str += 'op1:\n' + this.op1.toString();
		str += 'op2:\n' + this.op2.toString();
		return str;
	};

    return Channel2op;
})(Channel);
