var OPL3JSUtil = (function() {
    function OPL3JSUtil() {
    }

    /**
     * Creates an array of the given length, and sets the initial value of each element to "value".
     * @param length
     * @param value
     * @return {Array}
     */
    OPL3JSUtil.initArray = function(length, value) {
        var newArray = [];
        for (var i = 0; i < length; i ++) {
            newArray[i] = value;
        }
        return newArray;
    };

    /**
     * Replaces all values in an array with the given value.
     * @param array
     * @param value
     * @returns void
     */
    OPL3JSUtil.fillArray = function(array, value) {
        for (var i = 0; i < array.length; i ++) {
            array[i] = value;
        }
    };

    /**
     * Required for compatibility with the Java implementation.
     * OPL3 uses "Infinity" values (which are doubles) and casts them to ints.
     * In Javascript, these ints are still "Infinity", but in Java they are 2147483647, the max int value.
     */
    OPL3JSUtil.castToJavaInt = function(in_value) {
        if (in_value == Infinity) {
            return 2147483647;
        }
        return parseInt(in_value);
    };

    // These two methods are taken from CoffeeScript.
    OPL3JSUtil.__hasProp = {}.hasOwnProperty;
    OPL3JSUtil.__extends = function (child, parent) {
        for (var key in parent) {
            if (OPL3JSUtil.__hasProp.call(parent, key)) child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    };

    OPL3JSUtil.OPL3JSException = function(message) {
        this.message = message;
        this.name = 'OPL3JSException';
    };

    /* Read a file using xmlhttprequest

     If the HTML file with your javascript app has been saved to disk,
     this is an easy way to read in a data file. Writing out is
     more complicated and requires either an ActiveX object (IE)
     or XPCOM (Mozilla).

     fname - relative path to the file
     callback - function to call with file text
     */
    OPL3JSUtil.readFileHttp = function(fname, callback) {
        xmlhttp = getXmlHttp();
        xmlhttp.onload = function(e) {
            var uInt8Array = new Uint8Array(this.response);
            return callback(uInt8Array);
        };
        xmlhttp.open("GET", fname, true);
        xmlhttp.responseType = 'arraybuffer';
        xmlhttp.send(null);
    };

    /*
     Return a cross-browser xmlhttp request object
     */
    OPL3JSUtil.getXmlHttp = function() {
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if (xmlhttp == null) {
            alert("Your browser does not support XMLHTTP.");
        }
        return xmlhttp;
    };

    return OPL3JSUtil;
})();
