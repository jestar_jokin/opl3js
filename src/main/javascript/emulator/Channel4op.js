//import "OPL3JSUtil.js"
//import "EnvelopeGenerator.js"
//import "Channel.js"
//import "Operator.js"
//import "ChannelData.js"

var Channel4op;

Channel4op = (function(_super) {

    OPL3JSUtil.__extends(Channel4op, _super);

    function Channel4op(opl3Chip, baseAddress, o1, o2, o3, o4) {
        this.op1 = o1;
        this.op2 = o2;
        this.op3 = o3;
        this.op4 = o4;
        return Channel4op.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
    }

    Channel4op.prototype.getChannelOutput = function() {
        var channelOutput = 0,
            op1Output = 0, op2Output = 0, op3Output = 0, op4Output = 0; // double

        var output; // double[]

        var secondChannelBaseAddress = this.channelBaseAddress + 3; // int
        var secondCnt = this.opl3Chip.registers[secondChannelBaseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset] & 0x1; // int
        var cnt4op = (this.cnt << 1) | secondCnt; // int

        var feedbackOutput = (this.feedback[0] + this.feedback[1]) / 2;

        switch (cnt4op) {
            case 0:
                if (this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);
                op2Output = this.op2.getOperatorOutput(op1Output * Channel.toPhase);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);
                channelOutput = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                break;
            case 1:
                if (this.op2.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);
                op2Output = this.op2.getOperatorOutput(op1Output * Channel.toPhase);

                op3Output = this.op3.getOperatorOutput(Operator.noModulator);
                op4Output = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                channelOutput = (op2Output + op4Output) / 2;
                break;
            case 2:
                if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);

                op2Output = this.op2.getOperatorOutput(Operator.noModulator);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);
                op4Output = this.op4.getOperatorOutput(op3Output * Channel.toPhase);

                channelOutput = (op1Output + op4Output) / 2;
                break;
            case 3:
                if (this.op1.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op3.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF &&
                    this.op4.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF)
                    return this._getInFourChannels(0);

                op1Output = this.op1.getOperatorOutput(feedbackOutput);

                op2Output = this.op2.getOperatorOutput(Operator.noModulator);
                op3Output = this.op3.getOperatorOutput(op2Output * Channel.toPhase);

                op4Output = this.op4.getOperatorOutput(Operator.noModulator);

                channelOutput = (op1Output + op3Output + op4Output) / 3;
        }

        this.feedback[0] = this.feedback[1];
        this.feedback[1] = (op1Output * ChannelData.feedback[this.fb]) % 1;

        output = this._getInFourChannels(channelOutput);
        return output;
    };

    Channel4op.prototype._keyOn = function() {
        this.op1._keyOn();
        this.op2._keyOn();
        this.op3._keyOn();
        this.op4._keyOn();
        this.feedback[0] = this.feedback[1] = 0;
    };

    Channel4op.prototype._keyOff = function() {
        this.op1._keyOff();
        this.op2._keyOff();
        this.op3._keyOff();
        this.op4._keyOff();
    };

    Channel4op.prototype._updateOperators = function() {
        // Key Scale Number, used in EnvelopeGenerator.setActualRates().
        var keyScaleNumber = this.block * 2 + ((this.fnumh >> this.opl3Chip.nts) & 0x01);
        var f_number = (this.fnumh << 8) | this.fnuml;
        this.op1._updateOperator(keyScaleNumber, f_number, this.block);
        this.op2._updateOperator(keyScaleNumber, f_number, this.block);
        this.op3._updateOperator(keyScaleNumber, f_number, this.block);
        this.op4._updateOperator(keyScaleNumber, f_number, this.block);
    };

    Channel4op.prototype.toString = function() {
        var str = '';
        var f_number = (this.fnumh << 8) + this.fnuml;
        str += 'channelBaseAddress: ' + this.channelBaseAddress +'\n';
        str += 'f_number: ' + f_number + ', block: ' + this.block + '\n';
        str += 'cnt: ' + this.cnt + ', feedback: ' + this.fb + '\n';
        str += 'op1:\n' + this.op1.toString();
        str += 'op2:\n' + this.op2.toString();
        str += 'op3:\n' + this.op3.toString();
        str += 'op4:\n' + this.op4.toString();
        return str;
    };

    return Channel4op;
})(Channel);