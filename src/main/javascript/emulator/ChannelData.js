var ChannelData = (function () {
    function ChannelData() {
    }

    ChannelData._2_KON1_BLOCK3_FNUMH2_Offset = 0xB0;
    ChannelData.FNUML8_Offset = 0xA0;
    ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset = 0xC0;
    // Feedback rate in fractions of 2*Pi, normalized to (0,1):
    // 0, Pi/16, Pi/8, Pi/4, Pi/2, Pi, 2*Pi, 4*Pi turns to be:
    ChannelData.feedback = [0, 1.0 / 32.0, 1.0 / 16.0, 1.0 / 8.0, 1.0 / 4.0, 1.0 / 2.0, 1, 2];

    return ChannelData;
})();