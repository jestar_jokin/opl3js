//import "OPL3JSUtil.js"
//import "OPL3Data.js"
//import "Operator.js"
//import "rhythm/HighHatOperator.js"
//import "rhythm/SnareDrumOperator.js"
//import "rhythm/TomTomOperator.js"
//import "rhythm/TopCymbalOperator.js"
//import "Channel2op.js"
//import "Channel4op.js"
//import "rhythm/BassDrumChannel.js"
//import "rhythm/HighHatSnareDrumChannel.js"
//import "rhythm/TomTomTopCymbalChannel.js"
//import "DisabledChannel.js"
//import "ChannelData.js"

var OPL3;

OPL3 = (function() {
    function OPL3() {
        // Constructor stuff.
        this.registers = OPL3JSUtil.initArray(0x200, 0); // int[]

        this.operators = []; // Operator[][]
        this.channels2op = []; // Channel2op[][]
        this.channels4op = []; //Channel4op[][]
        this.channels = []; // Channel[][]
        this.disabledChannel = null; // DisabledChannel

        this.bassDrumChannel = null; // BassDrumChannel
        this.highHatSnareDrumChannel = null; // HighHatSnareDrumChannel
        this.tomTomTopCymbalChannel = null; // TomTomTopCymbalChannel
        this.highHatOperator = null; // HighHatOperator
        this.snareDrumOperator = null; // SnareDrumOperator
        this.tomTomOperator = null; // TomTomOperator
        this.topCymbalOperator = null; // TopCymbalOperator
        this.highHatOperatorInNonRhythmMode = null; // Operator
        this.snareDrumOperatorInNonRhythmMode = null; // Operator
        this.tomTomOperatorInNonRhythmMode = null; // Operator
        this.topCymbalOperatorInNonRhythmMode = null; // Operator
        this.nts = this.dam = this.dvb = this.ryt = this.bd = this.sd = this.tom = this.tc = this.hh = this._new = this.connectionsel = 0; // int
        this.vibratoIndex = this.tremoloIndex = 0; // int
        this.channels = [OPL3JSUtil.initArray(9, null), OPL3JSUtil.initArray(9, null)];
        this.output = OPL3JSUtil.initArray(4, 0); // short[]
        this.outputBuffer = OPL3JSUtil.initArray(4, 0.0); // double[]
        this._initOperators();
        this._initChannels2op();
        this._initChannels4op();
        this._initRhythmChannels();
        this._initChannels();
    }

    // ########
    // Static properties
    // ########

    OPL3.OutputFormat = {
        INT16 : 'INT16',
        FLOAT32 : 'FLOAT32'
    };

    // ########
    // Public methods
    // ########
    // The methods read() and write() are the only
    // ones needed by the user to interface with the emulator.
    // read() returns one frame at a time, to be played at 49700 Hz,
    // with each frame being four 16-bit samples (or 32-bit floats),
    // corresponding to the OPL3 four output channels CHA...CHD.
    OPL3.prototype.read = function(outputFormat) {
        if (outputFormat === undefined) {
            outputFormat = OPL3.OutputFormat.INT16;
        }

        var output = this.output;
        OPL3JSUtil.fillArray(output, 0); // short[]
        var outputBuffer = this.outputBuffer;
        OPL3JSUtil.fillArray(outputBuffer, 0.0); // double[]
        var channelOutput = null; // double[]

        // If _new = 0, use OPL2 mode with 9 channels. If _new = 1, use OPL3 18 channels;
        for (var array = 0; array < (this._new + 1); array++) {
            for (var channelNumber = 0; channelNumber < 9; channelNumber++) {
                // Reads output from each OPL3 channel, and accumulates it in the output buffer:
                channelOutput = this.channels[array][channelNumber].getChannelOutput();
                for (var outputChannelNumber2 = 0; outputChannelNumber2 < 4; outputChannelNumber2++) {
                    outputBuffer[outputChannelNumber2] += channelOutput[outputChannelNumber2];
                }
            }
        }

        // Normalizes the output buffer after all channels have been added,
        // with a maximum of 18 channels,
        // and multiplies it to get the 16 bit signed output.
        for (var outputChannelNumber3 = 0; outputChannelNumber3 < 4; outputChannelNumber3++) {
            var tmpValue = outputBuffer[outputChannelNumber3] / 18.0;
            if (outputFormat === OPL3.OutputFormat.FLOAT32) {
                // do nothing
            } else {
                tmpValue = parseInt(tmpValue * 0x7FFF);
            }
            output[outputChannelNumber3] = tmpValue;
        }

        // Advances the OPL3-wide vibrato index, which is used by
        // PhaseGenerator.getPhase() in each Operator.
        this.vibratoIndex++;
        if (this.vibratoIndex >= OPL3Data.vibratoTable[this.dvb].length) {
            this.vibratoIndex = 0;
        }
        // Advances the OPL3-wide tremolo index, which is used by
        // EnvelopeGenerator.getEnvelope() in each Operator.
        this.tremoloIndex++;
        if (this.tremoloIndex >= OPL3Data.tremoloTable[this.dam].length) {
            this.tremoloIndex = 0;
        }

        return output;
    };

    OPL3.prototype.write = function(array, address, data) {
        // The OPL3 has two registers arrays, each with adresses ranging
        // from 0x00 to 0xF5.
        // This emulator uses one array, with the two original register arrays
        // starting at 0x00 and at 0x100.
        var registerAddress = (array << 8) | address; // int
        // If the address is out of the OPL3 memory map, returns.
        if (registerAddress < 0 || registerAddress >= 0x200) return;

        this.registers[registerAddress] = data;
        switch (address & 0xE0) {
            // The first 3 bits masking gives the type of the register by using its base address:
            // 0x00, 0x20, 0x40, 0x60, 0x80, 0xA0, 0xC0, 0xE0
            // When it is needed, we further separate the register type inside each base address,
            // which is the case of 0x00 and 0xA0.

            // Through out this emulator we will use the same name convention to
            // reference a byte with several bit registers.
            // The name of each bit register will be followed by the number of bits
            // it occupies inside the byte.
            // Numbers without accompanying names are unused bits.
            case 0x00:
                // Unique registers for the entire OPL3:
                if (array == 1) {
                    if (address == 0x04) {
                        this._update_2_CONNECTIONSEL6();
                    } else if (address == 0x05) {
                        this._update_7_NEW1();
                    }
                }
                else if (address == 0x08) {
                    this._update_1_NTS1_6();
                }
                break;

            case 0xA0:
                // 0xBD is a control register for the entire OPL3:
                if (address == 0xBD) {
                    if (array == 0) {
                        this._update_DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1();
                    }
                    break;
                }
                // Registers for each channel are in A0-A8, B0-B8, C0-C8, in both register arrays.
                // 0xB0...0xB8 keeps kon,block,fnum(h) for each channel.
                if ((address & 0xF0) == 0xB0 && address <= 0xB8) {
                    // If the address is in the second register array, adds 9 to the channel number.
                    // The channel number is given by the last four bits, like in A0,...,A8.
                    this.channels[array][address & 0x0F].update_2_KON1_BLOCK3_FNUMH2();
                    break;
                }
                // 0xA0...0xA8 keeps fnum(l) for each channel.
                if ((address & 0xF0) == 0xA0 && address <= 0xA8)
                    this.channels[array][address & 0x0F].update_FNUML8();
                break;
            // 0xC0...0xC8 keeps cha,chb,chc,chd,fb,cnt for each channel:
            case 0xC0:
                if (address <= 0xC8)
                    this.channels[array][address & 0x0F].update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
                break;

            // Registers for each of the 36 Operators:
            default:
                var operatorOffset = address & 0x1F;
                if (this.operators[array][operatorOffset] == null) {
                    break;
                }
                switch (address & 0xE0) {
                    // 0x20...0x35 keeps am,vib,egt,ksr,mult for each operator:
                    case 0x20:
                        this.operators[array][operatorOffset].update_AM1_VIB1_EGT1_KSR1_MULT4();
                        break;
                    // 0x40...0x55 keeps ksl,tl for each operator:
                    case 0x40:
                        this.operators[array][operatorOffset].update_KSL2_TL6();
                        break;
                    // 0x60...0x75 keeps ar,dr for each operator:
                    case 0x60:
                        this.operators[array][operatorOffset].update_AR4_DR4();
                        break;
                    // 0x80...0x95 keeps sl,rr for each operator:
                    case 0x80:
                        this.operators[array][operatorOffset].update_SL4_RR4();
                        break;
                    // 0xE0...0xF5 keeps ws for each operator:
                    case 0xE0:
                        this.operators[array][operatorOffset].update_5_WS3();
                }
        }
    };

    // ########
    // Private methods
    // ########
    OPL3.prototype._initOperators = function() {
        var baseAddress;
        // The YMF262 has 36 operators:
        this.operators = [
            OPL3JSUtil.initArray(0x20, null),
            OPL3JSUtil.initArray(0x20, null)
        ];
        for (var array = 0; array < 2; array++) {
            for (var group = 0; group <= 0x10; group += 8) {
                for (var offset = 0; offset < 6; offset++) {
                    baseAddress = (array << 8) | (group + offset);
                    this.operators[array][group + offset] = new Operator(this, baseAddress);
                }
            }
        }

        // Create specific operators to switch when in rhythm mode:
        this.highHatOperator = new HighHatOperator(this);
        this.snareDrumOperator = new SnareDrumOperator(this);
        this.tomTomOperator = new TomTomOperator(this);
        this.topCymbalOperator = new TopCymbalOperator(this);

        // Save operators when they are in non-rhythm mode:
        // Channel 7:
        this.highHatOperatorInNonRhythmMode = this.operators[0][0x11];
        this.snareDrumOperatorInNonRhythmMode = this.operators[0][0x14];
        // Channel 8:
        this.tomTomOperatorInNonRhythmMode = this.operators[0][0x12];
        this.topCymbalOperatorInNonRhythmMode = this.operators[0][0x15];
    };

    OPL3.prototype._initChannels2op = function() {
        // The YMF262 has 18 2-op channels.
        // Each 2-op channel can be at a serial or parallel operator configuration:
        this.channels2op = [
            OPL3JSUtil.initArray(9, null),
            OPL3JSUtil.initArray(9, null)
        ]; // Channel2op[2][9]

        for (var array = 0; array < 2; array++) {
            for (var channelNumber = 0; channelNumber < 3; channelNumber++) {
                var baseAddress = (array << 8) | channelNumber;
                // Channels 1, 2, 3 -> Operator offsets 0x0,0x3; 0x1,0x4; 0x2,0x5
                this.channels2op[array][channelNumber] = new Channel2op(this, baseAddress, this.operators[array][channelNumber], this.operators[array][channelNumber + 0x3]);
                // Channels 4, 5, 6 -> Operator offsets 0x8,0xB; 0x9,0xC; 0xA,0xD
                this.channels2op[array][channelNumber + 3] = new Channel2op(this, baseAddress + 3, this.operators[array][channelNumber + 0x8], this.operators[array][channelNumber + 0xB]);
                // Channels 7, 8, 9 -> Operators 0x10,0x13; 0x11,0x14; 0x12,0x15
                this.channels2op[array][channelNumber + 6] = new Channel2op(this, baseAddress + 6, this.operators[array][channelNumber + 0x10], this.operators[array][channelNumber + 0x13]);
            }
        }
    };

    OPL3.prototype._initChannels4op = function() {
        // The YMF262 has 3 4-op channels in each array:
        this.channels4op = [
            OPL3JSUtil.initArray(3, null),
            OPL3JSUtil.initArray(3, null)
        ]; //Channel4op[2][3];
        for (var array = 0; array < 2; array++) {
            for (var channelNumber = 0; channelNumber < 3; channelNumber++) {
                var baseAddress = (array << 8) | channelNumber;
                // Channels 1, 2, 3 -> Operators 0x0,0x3,0x8,0xB; 0x1,0x4,0x9,0xC; 0x2,0x5,0xA,0xD;
                this.channels4op[array][channelNumber] = new Channel4op(this, baseAddress, this.operators[array][channelNumber], this.operators[array][channelNumber + 0x3], this.operators[array][channelNumber + 0x8], this.operators[array][channelNumber + 0xB]);
            }
        }
    };

    OPL3.prototype._initRhythmChannels = function() {
        this.bassDrumChannel = new BassDrumChannel(this);
        this.highHatSnareDrumChannel = new HighHatSnareDrumChannel(this);
        this.tomTomTopCymbalChannel = new TomTomTopCymbalChannel(this);
    };

    OPL3.prototype._initChannels = function() {
        // Channel is an abstract class that can be a 2-op, 4-op, rhythm or disabled channel,
        // depending on the OPL3 configuration at the time.
        // channels[] inits as a 2-op serial channel array:
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 9; i++) {
                this.channels[array][i] = this.channels2op[array][i];
            }
        }

        // Unique instance to fill future gaps in the Channel array,
        // when there will be switches between 2op and 4op mode.
        this.disabledChannel = new DisabledChannel(this);
    };

    OPL3.prototype._update_1_NTS1_6 = function() {
        var _1_nts1_6 = this.registers[OPL3Data._1_NTS1_6_Offset];
        // Note Selection. This register is used in Channel.updateOperators() implementations,
        // to calculate the channel´s Key Scale Number.
        // The value of the actual envelope rate follows the value of
        // OPL3.nts,Operator.keyScaleNumber and Operator.ksr
        this.nts = (_1_nts1_6 & 0x40) >> 6;
    };

    OPL3.prototype._update_DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1 = function() {
        var dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 = this.registers[OPL3Data.DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1_Offset];
        // Depth of amplitude. This register is used in EnvelopeGenerator.getEnvelope();
        this.dam = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x80) >> 7;

        // Depth of vibrato. This register is used in PhaseGenerator.getPhase();
        this.dvb = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x40) >> 6;

        var new_ryt = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x20) >> 5;
        if (new_ryt != this.ryt) {
            this.ryt = new_ryt;
            this._setRhythmMode();
        }

        var new_bd = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x10) >> 4;
        if (new_bd != this.bd) {
            this.bd = new_bd;
            if (this.bd == 1) {
                this.bassDrumChannel.op1._keyOn();
                this.bassDrumChannel.op2._keyOn();
            }
        }

        var new_sd = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x08) >> 3;
        if (new_sd != this.sd) {
            this.sd = new_sd;
            if (this.sd == 1) {
                this.snareDrumOperator._keyOn();
            }
        }

        var new_tom = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x04) >> 2;
        if (new_tom != this.tom) {
            this.tom = new_tom;
            if (this.tom == 1)  {
                this.tomTomOperator._keyOn();
            }
        }

        var new_tc = (dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x02) >> 1;
        if (new_tc != this.tc) {
            this.tc = new_tc;
            if (this.tc == 1) {
                this.topCymbalOperator._keyOn();
            }
        }

        var new_hh = dam1_dvb1_ryt1_bd1_sd1_tom1_tc1_hh1 & 0x01;
        if (new_hh != this.hh) {
            this.hh = new_hh;
            if (this.hh == 1) {
                this.highHatOperator._keyOn();
            }
        }
    };

    OPL3.prototype._update_7_NEW1 = function() {
        var _7_new1 = this.registers[OPL3Data._7_NEW1_Offset];
        // OPL2/OPL3 mode selection. This register is used in
        // OPL3.read(), OPL3.write() and Operator.getOperatorOutput();
        this._new = (_7_new1 & 0x01);
        if (this._new == 1) {
            this._setEnabledChannels();
        }
        this._set4opConnections();
    };

    OPL3.prototype._setEnabledChannels = function() {
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 9; i++) {
                var baseAddress = this.channels[array][i].channelBaseAddress;
                this.registers[baseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset] |= 0xF0;
                this.channels[array][i].update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
            }
        }
    };

    OPL3.prototype._update_2_CONNECTIONSEL6 = function() {
        // This method is called only if _new is set.
        var _2_connectionsel6 = this.registers[OPL3Data._2_CONNECTIONSEL6_Offset];
        // 2-op/4-op channel selection. This register is used here to configure the OPL3.channels[] array.
        this.connectionsel = (_2_connectionsel6 & 0x3F);
        this._set4opConnections();
    };

    OPL3.prototype._set4opConnections = function() {
        // bits 0, 1, 2 sets respectively 2-op channels (1,4), (2,5), (3,6) to 4-op operation.
        // bits 3, 4, 5 sets respectively 2-op channels (10,13), (11,14), (12,15) to 4-op operation.
        for (var array = 0; array < 2; array++) {
            for (var i = 0; i < 3; i++) {
                if (this._new == 1) {
                    var shift = array * 3 + i;
                    var connectionBit = (this.connectionsel >> shift) & 0x01;
                    if (connectionBit == 1) {
                        this.channels[array][i] = this.channels4op[array][i];
                        this.channels[array][i + 3] = this.disabledChannel;
                        this.channels[array][i].updateChannel();
                        continue;
                    }
                }
                this.channels[array][i] = this.channels2op[array][i];
                this.channels[array][i + 3] = this.channels2op[array][i + 3];
                this.channels[array][i].updateChannel();
                this.channels[array][i + 3].updateChannel();
            }
        }
    };

    OPL3.prototype._setRhythmMode = function() {
        var i;
        if (this.ryt == 1) {
            this.channels[0][6] = this.bassDrumChannel;
            this.channels[0][7] = this.highHatSnareDrumChannel;
            this.channels[0][8] = this.tomTomTopCymbalChannel;
            this.operators[0][0x11] = this.highHatOperator;
            this.operators[0][0x14] = this.snareDrumOperator;
            this.operators[0][0x12] = this.tomTomOperator;
            this.operators[0][0x15] = this.topCymbalOperator;
        }
        else {
            for (i = 6; i <= 8; i++) {
                this.channels[0][i] = this.channels2op[0][i];
            }
            this.operators[0][0x11] = this.highHatOperatorInNonRhythmMode;
            this.operators[0][0x14] = this.snareDrumOperatorInNonRhythmMode;
            this.operators[0][0x12] = this.tomTomOperatorInNonRhythmMode;
            this.operators[0][0x15] = this.topCymbalOperatorInNonRhythmMode;
        }
        for(i = 6; i <= 8; i++) {
            this.channels[0][i].updateChannel();
        }
    };

    return OPL3;
})();

