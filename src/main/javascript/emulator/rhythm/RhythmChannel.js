//import "../OPL3JSUtil.js"
//import "../Operator.js"
//import "../Channel2op.js"

var RhythmChannel;

RhythmChannel = (function(_super) {

    OPL3JSUtil.__extends(RhythmChannel, _super);

	function RhythmChannel(opl3Chip, baseAddress, o1, o2) {
		return RhythmChannel.__super__.constructor.apply(this, [
            opl3Chip,
            baseAddress,
            o1,
            o2
        ]);
	}

    RhythmChannel.prototype.getChannelOutput = function() {
		var channelOutput = 0, op1Output = 0, op2Output = 0; // double
		var output; // double[]

		// Note that, different from the common channel,
		// we do not check to see if the Operator's envelopes are Off.
		// Instead, we always do the calculations,
		// to update the publicly available phase.
		op1Output = this.op1.getOperatorOutput(Operator.noModulator);
		op2Output = this.op2.getOperatorOutput(Operator.noModulator);
		channelOutput = (op1Output + op2Output) / 2;

		output = this._getInFourChannels(channelOutput);
		return output;
	};

	// Rhythm channels are always running,
	// only the envelope is activated by the user.
    RhythmChannel.prototype._keyOn = function() {};
    RhythmChannel.prototype._keyOff = function() {};

    return RhythmChannel;
})(Channel2op);

