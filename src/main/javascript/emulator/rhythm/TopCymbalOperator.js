//import "../OPL3JSUtil.js"
//import "../Operator.js"
//import "../OperatorData.js"

var TopCymbalOperator;

TopCymbalOperator = (function(_super) {

    OPL3JSUtil.__extends(TopCymbalOperator, _super);

	function TopCymbalOperator(opl3Chip, baseAddress) {
		if (baseAddress === undefined) {
			baseAddress = TopCymbalOperator.topCymbalOperatorBaseAddress;
		}
		return TopCymbalOperator.__super__.constructor.apply(this, [opl3Chip, baseAddress]);
	}

	TopCymbalOperator.topCymbalOperatorBaseAddress = 0x15;

	// This method is used here with the HighHatOperator phase
	// as the externalPhase.
	// Conversely, this method is also used through inheritance by the HighHatOperator,
	// now with the TopCymbalOperator phase as the externalPhase.
	TopCymbalOperator.prototype.getOperatorOutput = function(modulator, externalPhase) {
		if (externalPhase === undefined) {
			// The Top Cymbal operator uses his own phase together with the High Hat phase.
			externalPhase = this.opl3Chip.highHatOperator.phase * OperatorData.multTable[this.opl3Chip.highHatOperator.mult]; // double
		}
		var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
		this.envelope = Math.pow(10, envelopeInDB / 10.0);

		this.phase = this.phaseGenerator.getPhase(this.vib);

		var waveIndex = this.ws & ((this.opl3Chip._new << 2) + 3); // int
		var waveform = OperatorData.waveforms[waveIndex]; // double[]

		// Empirically tested multiplied phase for the Top Cymbal:
		var carrierPhase = (8 * this.phase) % 1; // double
		var modulatorPhase = externalPhase; // double
		var modulatorOutput = this._getOutput(Operator.noModulator, modulatorPhase, waveform); // double
		var carrierOutput = this._getOutput(modulatorOutput, carrierPhase, waveform); // double

		var cycles = 4; // int
		if((carrierPhase * cycles) % cycles > 0.1) {
			carrierOutput = 0;
		}

		return carrierOutput * 2;
	};

    return TopCymbalOperator;
})(Operator);
