//import "../OPL3JSUtil.js"
//import "RhythmChannel.js"

var TomTomTopCymbalChannel;

TomTomTopCymbalChannel = (function(_super) {

    OPL3JSUtil.__extends(TomTomTopCymbalChannel, _super);

	function TomTomTopCymbalChannel(opl3Chip) {
		return TomTomTopCymbalChannel.__super__.constructor.apply(this, [
            opl3Chip,
            TomTomTopCymbalChannel.tomTomTopCymbalChannelBaseAddress,
            opl3Chip.tomTomOperator,
            opl3Chip.topCymbalOperator
        ]);
	}

	TomTomTopCymbalChannel.tomTomTopCymbalChannelBaseAddress = 8;

    return TomTomTopCymbalChannel;
})(RhythmChannel);
