//import "../OPL3JSUtil.js"
//import "../OperatorData.js"
//import "TopCymbalOperator.js"

var HighHatOperator;

HighHatOperator = (function(_super) {

    OPL3JSUtil.__extends(HighHatOperator, _super);

	function HighHatOperator(opl3Chip) {
		return HighHatOperator.__super__.constructor.apply(this, [
            opl3Chip,
            HighHatOperator.highHatOperatorBaseAddress
        ]);
	}

	HighHatOperator.highHatOperatorBaseAddress = 0x11;

	HighHatOperator.prototype.getOperatorOutput = function(modulator) {
		var topCymbalOperatorPhase =
			this.opl3Chip.topCymbalOperator.phase * OperatorData.multTable[this.opl3Chip.topCymbalOperator.mult];
		// The sound output from the High Hat resembles the one from
		// Top Cymbal, so we use the parent method and modifies his output
		// accordingly afterwards.
		var operatorOutput = HighHatOperator.__super__.getOperatorOutput.call(this, modulator, topCymbalOperatorPhase);
		if (operatorOutput == 0) {
            operatorOutput = Math.random() * this.envelope;
        }
		return operatorOutput;
	};

    return HighHatOperator;
})(TopCymbalOperator);