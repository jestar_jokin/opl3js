//import "../OPL3JSUtil.js"
//import "../OperatorData.js"
//import "../Operator.js"
//import "../EnvelopeGenerator.js"

var SnareDrumOperator;

SnareDrumOperator = (function(_super) {

    OPL3JSUtil.__extends(SnareDrumOperator, _super);

	function SnareDrumOperator(opl3Chip) {
		return SnareDrumOperator.__super__.constructor.apply(this, [
            opl3Chip,
            SnareDrumOperator.snareDrumOperatorBaseAddress
        ]);
	}

	SnareDrumOperator.snareDrumOperatorBaseAddress = 0x14;

	SnareDrumOperator.prototype.getOperatorOutput = function(modulator) {
		if (this.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF) {
			return 0;
		}

		var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
		this.envelope = Math.pow(10, envelopeInDB / 10.0);

		// If it is in OPL2 mode, use first four waveforms only:
		var waveIndex = this.ws & ((this.opl3Chip._new << 2) + 3); // int
		var waveform = OperatorData.waveforms[waveIndex]; // double[]

		this.phase = this.opl3Chip.highHatOperator.phase * 2;

		var operatorOutput = this._getOutput(modulator, this.phase, waveform); // double

		var noise = Math.random() * this.envelope; // double

		if (operatorOutput / this.envelope != 1 && operatorOutput / this.envelope != -1) {
			if (operatorOutput > 0) {
				operatorOutput = noise;
			} else if (operatorOutput < 0) {
				operatorOutput = -noise;
			} else {
				operatorOutput = 0;
			}
		}
		return operatorOutput * 2;
	};

    return SnareDrumOperator;
})(Operator);