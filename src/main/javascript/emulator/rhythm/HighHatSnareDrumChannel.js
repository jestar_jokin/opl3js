//import "../OPL3JSUtil.js"
//import "RhythmChannel.js"

var HighHatSnareDrumChannel;

HighHatSnareDrumChannel = (function(_super) {

    OPL3JSUtil.__extends(HighHatSnareDrumChannel, _super);

	function HighHatSnareDrumChannel(opl3Chip) {
		return HighHatSnareDrumChannel.__super__.constructor.apply(this, [
            opl3Chip,
            HighHatSnareDrumChannel.highHatSnareDrumChannelBaseAddress,
            opl3Chip.highHatOperator,
            opl3Chip.snareDrumOperator
        ]);
	}

	HighHatSnareDrumChannel.highHatSnareDrumChannelBaseAddress = 7;

    return HighHatSnareDrumChannel;
})(RhythmChannel);

