//import "../OPL3JSUtil.js"
//import "../Operator.js"

var TomTomOperator;

TomTomOperator = (function(_super) {

    OPL3JSUtil.__extends(TomTomOperator, _super);

	function TomTomOperator(opl3Chip) {
		return TomTomOperator.__super__.constructor.apply(this, [
            opl3Chip,
            TomTomOperator.tomTomOperatorBaseAddress
        ]);
	}

	TomTomOperator.tomTomOperatorBaseAddress = 0x12;

    return TomTomOperator;
})(Operator);
