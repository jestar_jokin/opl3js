//import "../OPL3JSUtil.js"
//import "../Operator.js"
//import "../Channel2op.js"

var BassDrumChannel;

BassDrumChannel = (function(_super) {

    OPL3JSUtil.__extends(BassDrumChannel, _super);

	function BassDrumChannel(opl3Chip) {
		return BassDrumChannel.__super__.constructor.apply(this, [
            opl3Chip,
            BassDrumChannel.bassDrumChannelBaseAddress,
			new Operator(opl3Chip, BassDrumChannel.op1BaseAddress),
			new Operator(opl3Chip, BassDrumChannel.op2BaseAddress)
        ]);
	}

	BassDrumChannel.bassDrumChannelBaseAddress = 6;
	BassDrumChannel.op1BaseAddress = 0x10;
	BassDrumChannel.op2BaseAddress = 0x13;

	BassDrumChannel.prototype.getChannelOutput = function() {
		// Bass Drum ignores first operator, when it is in series.
		if (this.cnt == 1) {
			this.op1.ar = 0;
		}
		return BassDrumChannel.__super__.getChannelOutput.call(this);
	};

	// Key ON and OFF are unused in rhythm channels.
	BassDrumChannel.prototype._keyOn = function() {};
	BassDrumChannel.prototype._keyOff = function() {};

    return BassDrumChannel;
})(Channel2op);
