//import "OPL3JSUtil.js"
//import "OPL3Data.js"
//import "PhaseGenerator.js"
//import "EnvelopeGenerator.js"
//import "OperatorData.js"

var Operator;

Operator = (function() {
    function Operator(opl3Chip, baseAddress) {
        this.opl3Chip = opl3Chip;
        this.operatorBaseAddress = baseAddress;
        this.phaseGenerator = new PhaseGenerator(opl3Chip);
        this.envelopeGenerator = new EnvelopeGenerator(opl3Chip);

        this.envelope = 0.0;
        this.phase = 0.0;
        this.am = this.vib = this.ksr = this.egt = this.mult = this.ksl = this.tl = this.ar = this.dr = this.sl = this.rr = this.ws = 0;
        this.keyScaleNumber = this.f_number = this.block = 0;
    }

    // ########
    // Static properties
    // ########
    Operator.noModulator = 0;

    // ########
    // Public methods
    // ########
    Operator.prototype.update_AM1_VIB1_EGT1_KSR1_MULT4 = function() {
        var am1_vib1_egt1_ksr1_mult4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.AM1_VIB1_EGT1_KSR1_MULT4_Offset];

        // Amplitude Modulation. This register is used int EnvelopeGenerator.getEnvelope();
        this.am  = (am1_vib1_egt1_ksr1_mult4 & 0x80) >> 7;
        // Vibrato. This register is used in PhaseGenerator.getPhase();
        this.vib = (am1_vib1_egt1_ksr1_mult4 & 0x40) >> 6;
        // Envelope Generator Type. This register is used in EnvelopeGenerator.getEnvelope();
        this.egt = (am1_vib1_egt1_ksr1_mult4 & 0x20) >> 5;
        // Key Scale Rate. Sets the actual envelope rate together with rate and keyScaleNumber.
        // This register os used in EnvelopeGenerator.setActualAttackRate().
        this.ksr = (am1_vib1_egt1_ksr1_mult4 & 0x10) >> 4;
        // Multiple. Multiplies the Channel.baseFrequency to get the Operator.operatorFrequency.
        // This register is used in PhaseGenerator.setFrequency().
        this.mult = am1_vib1_egt1_ksr1_mult4 & 0x0F;

        this.phaseGenerator.setFrequency(this.f_number, this.block, this.mult);
        this.envelopeGenerator.setActualAttackRate(this.ar, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualDecayRate(this.dr, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualReleaseRate(this.rr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_KSL2_TL6 = function() {
        var ksl2_tl6 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.KSL2_TL6_Offset];

        // Key Scale Level. Sets the attenuation in accordance with the octave.
        this.ksl = (ksl2_tl6 & 0xC0) >> 6;
        // Total Level. Sets the overall damping for the envelope.
        this.tl =  ksl2_tl6 & 0x3F;

        this.envelopeGenerator.setAtennuation(this.f_number, this.block, this.ksl);
        this.envelopeGenerator.setTotalLevel(this.tl);
    };

    Operator.prototype.update_AR4_DR4 = function() {
        var ar4_dr4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.AR4_DR4_Offset];

        // Attack Rate.
        this.ar = (ar4_dr4 & 0xF0) >> 4;
        // Decay Rate.
        this.dr =  ar4_dr4 & 0x0F;

        this.envelopeGenerator.setActualAttackRate(this.ar, this.ksr, this.keyScaleNumber);
        this.envelopeGenerator.setActualDecayRate(this.dr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_SL4_RR4 = function() {
        var sl4_rr4 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData.SL4_RR4_Offset];

        // Sustain Level.
        this.sl = (sl4_rr4 & 0xF0) >> 4;
        // Release Rate.
        this.rr = sl4_rr4 & 0x0F;

        this.envelopeGenerator.setActualSustainLevel(this.sl);
        this.envelopeGenerator.setActualReleaseRate(this.rr, this.ksr, this.keyScaleNumber);
    };

    Operator.prototype.update_5_WS3 = function() {
        var _5_ws3 = this.opl3Chip.registers[this.operatorBaseAddress + OperatorData._5_WS3_Offset];
        this.ws =  _5_ws3 & 0x07;
    };

    Operator.prototype.getOperatorOutput = function(modulator) {
        if (this.envelopeGenerator.stage == EnvelopeGenerator.Stage.OFF) {
            return 0;
        }

        var envelopeInDB = this.envelopeGenerator.getEnvelope(this.egt, this.am); // double
        this.envelope = Math.pow(10, envelopeInDB / 10.0);

        // If it is in OPL2 mode, use first four waveforms only:
        this.ws &= ((this.opl3Chip._new << 2) + 3);
        var waveform = OperatorData.waveforms[this.ws]; // double[]

        this.phase = this.phaseGenerator.getPhase(this.vib);

        var operatorOutput = this._getOutput(modulator, this.phase, waveform); // double
        return operatorOutput;
    };

    Operator.prototype.toString = function() {
        var str = '';
        var operatorFrequency = this.f_number * Math.pow(2, this.block - 1) * OPL3Data.sampleRate / Math.pow(2, 19) * OperatorData.multTable[this.mult];
        str += 'operatorBaseAddress: ' + this.operatorBaseAddress + '\n';
        str += 'operatorFrequency: ' + operatorFrequency + '\n';
        str += 'mult: ' + this.mult;
        str += 'ar: ' + this.ar;
        str += 'dr: ' + this.dr;
        str += 'sl: ' + this.sl;
        str += 'rr: ' + this.rr;
        str += 'ws: ' + this.ws + '\n';
        str += 'am: ' + this.am;
        str += 'vib: ' + this.vib;
        str += 'ksr: ' + this.ksr;
        str += 'egt: ' + this.egt;
        str += 'ksl: ' + this.ksl;
        str += 'tl: ' + this.tl + '\n';
        return str;
    };

    // ########
    // Protected methods
    // ########
    Operator.prototype._getOutput = function(modulator, outputPhase, waveform) {
        outputPhase = (outputPhase + modulator) % 1;
        if (outputPhase < 0) {
            outputPhase++;
            // If the double could not afford to be less than 1:
            outputPhase %= 1;
        }
        var sampleIndex = parseInt(outputPhase * OperatorData.waveLength);
        return waveform[sampleIndex] * this.envelope;
    };

    Operator.prototype._keyOn = function() {
        if (this.ar > 0) {
            this.envelopeGenerator.keyOn();
            this.phaseGenerator.keyOn();
        }
        else {
            this.envelopeGenerator.stage = EnvelopeGenerator.Stage.OFF;
        }
    };

    Operator.prototype._keyOff = function() {
        this.envelopeGenerator.keyOff();
    };

    Operator.prototype._updateOperator = function(ksn, f_num, blk) {
        this.keyScaleNumber = ksn;
        this.f_number = f_num;
        this.block = blk;
        this.update_AM1_VIB1_EGT1_KSR1_MULT4();
        this.update_KSL2_TL6();
        this.update_AR4_DR4();
        this.update_SL4_RR4();
        this.update_5_WS3();
    };

    return Operator;
})();

