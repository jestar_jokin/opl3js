//import "OPL3JSUtil.js"

var DisabledChannel;

DisabledChannel = (function(_super) {

    OPL3JSUtil.__extends(DisabledChannel, _super);

    function DisabledChannel(opl3Chip) {
        return DisabledChannel.__super__.constructor.apply(this, [opl3Chip, 0]);
    }

    DisabledChannel.prototype.getChannelOutput = function() {
        return this.getInFourChannels(0);
    };

    DisabledChannel.prototype._keyOn = function() { };
    DisabledChannel.prototype._keyOff = function() { };
    DisabledChannel.prototype._updateOperators = function() { };

    return DisabledChannel;
})(Channel);