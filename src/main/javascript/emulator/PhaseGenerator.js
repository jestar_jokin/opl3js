//import "OPL3Data.js"
//import "OperatorData.js"

var PhaseGenerator;

PhaseGenerator = (function() {
	function PhaseGenerator(opl3Chip) {
        this.opl3Chip = opl3Chip;
        this.phase = this.phaseIncrement = 0;
	}

	PhaseGenerator.prototype.setFrequency = function(f_number, block, mult) {
		// This frequency formula is derived from the following equation:
		// f_number = baseFrequency * pow(2,19) / sampleRate / pow(2,block-1);
		var baseFrequency =
			f_number * Math.pow(2, block - 1) * OPL3Data.sampleRate / Math.pow(2, 19);
		var operatorFrequency = baseFrequency * OperatorData.multTable[mult];

		// phase goes from 0 to 1 at
		// period = (1/frequency) seconds ->
		// Samples in each period is (1/frequency)*sampleRate =
		// = sampleRate/frequency ->
		// So the increment in each sample, to go from 0 to 1, is:
		// increment = (1-0) / samples in the period ->
		// increment = 1 / (OPL3Data.sampleRate/operatorFrequency) ->
		this.phaseIncrement = operatorFrequency / OPL3Data.sampleRate;
	};

	PhaseGenerator.prototype.getPhase = function(vib) {
		if (vib == 1) {
			// phaseIncrement = (operatorFrequency * vibrato) / sampleRate
			this.phase += this.phaseIncrement * OPL3Data.vibratoTable[this.opl3Chip.dvb][this.opl3Chip.vibratoIndex];
		} else {
			// phaseIncrement = operatorFrequency / sampleRate
			this.phase += this.phaseIncrement;
		}
		this.phase %= 1;
		return this.phase;
	};

	PhaseGenerator.prototype.keyOn = function() {
		this.phase = 0;
	};

	PhaseGenerator.prototype.toString = function() {
		return 'Operator frequency: ' + OPL3Data.sampleRate * this.phaseIncrement + ' Hz.\n';
	};

    return PhaseGenerator;
})();