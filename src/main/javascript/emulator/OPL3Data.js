//import "OPL3JSUtil.js"

var OPL3Data = (function() {
    function OPL3Data() {
    }

    OPL3Data._1_NTS1_6_Offset = 0x08;
    OPL3Data.DAM1_DVB1_RYT1_BD1_SD1_TOM1_TC1_HH1_Offset = 0xBD;
    OPL3Data._7_NEW1_Offset = 0x105;
    OPL3Data._2_CONNECTIONSEL6_Offset = 0x104;

    OPL3Data.sampleRate = 49700; // not quite right, I think.

    OPL3Data.calculateIncrement = function(begin, end, period) {
        return (end - begin) / OPL3Data.sampleRate * (1 / period);
    };

    OPL3Data.vibratoTable = (function() {
        // According to the YMF262 datasheet, the OPL3 vibrato repetition rate is 6.1 Hz.
        // According to the YMF278B manual, it is 6.0 Hz.
        // The information that the vibrato table has 8 levels standing 1024 samples each
        // was taken from the emulator by Jarek Burczynski and Tatsuyuki Satoh,
        // with a frequency of 6,06689453125 Hz, what  makes sense with the difference
        // in the information on the datasheets.

        // The first array is used when DVB=0 and the second array is used when DVB=1.
        var vibratoTable = [
            OPL3JSUtil.initArray(8192, 0),
            OPL3JSUtil.initArray(8192, 0)
        ];

        var semitone = Math.pow(2, 1 / 12);
        // A cent is 1/100 of a semitone:
        var cent = Math.pow(semitone, 1 / 100);

        // When dvb=0, the depth is 7 cents, when it is 1, the depth is 14 cents.
        var DVB0 = Math.pow(cent, 7);
        var DVB1 = Math.pow(cent, 14);
        var i;
        for (i = 0; i < 1024; i++) {
            vibratoTable[0][i] = vibratoTable[1][i] = 1;
        }
        for (; i < 2048; i++) {
            vibratoTable[0][i] = Math.sqrt(DVB0);
            vibratoTable[1][i] = Math.sqrt(DVB1);
        }
        for (; i < 3072; i++) {
            vibratoTable[0][i] = DVB0;
            vibratoTable[1][i] = DVB1;
        }
        for (; i < 4096; i++) {
            vibratoTable[0][i] = Math.sqrt(DVB0);
            vibratoTable[1][i] = Math.sqrt(DVB1);
        }
        for (; i < 5120; i++) {
            vibratoTable[0][i] = vibratoTable[1][i] = 1;
        }
        for (; i < 6144; i++) {
            vibratoTable[0][i] = 1 / Math.sqrt(DVB0);
            vibratoTable[1][i] = 1 / Math.sqrt(DVB1);
        }
        for (; i < 7168; i++) {
            vibratoTable[0][i] = 1 / DVB0;
            vibratoTable[1][i] = 1 / DVB1;
        }
        for (; i < 8192; i++) {
            vibratoTable[0][i] = 1 / Math.sqrt(DVB0);
            vibratoTable[1][i] = 1 / Math.sqrt(DVB1);
        }
        return vibratoTable;
    })();


    OPL3Data.tremoloTable = (function() {
        var tremoloTable;

        // The OPL3 tremolo repetition rate is 3.7 Hz.
        var tremoloFrequency = 3.7;

        // The tremolo depth is -1 dB when DAM = 0, and -4.8 dB when DAM = 1.
        var tremoloDepth = [-1, -4.8];

        //  According to the YMF278B manual's OPL3 section graph,
        //              the tremolo waveform is not
        //   \      /   a sine wave, but a single triangle waveform.
        //    \    /    Thus, the period to achieve the tremolo depth is T/2, and
        //     \  /     the increment in each T/2 section uses a frequency of 2*f.
        //      \/      Tremolo varies from 0 dB to depth, to 0 dB again, at frequency*2:
        var tremoloIncrement = [
            OPL3Data.calculateIncrement(tremoloDepth[0], 0, 1 / (2 * tremoloFrequency)),
            OPL3Data.calculateIncrement(tremoloDepth[1], 0, 1 / (2 * tremoloFrequency))
        ];

        var tremoloTableLength = (OPL3Data.sampleRate / tremoloFrequency).toFixed();

        // First array used when AM = 0 and second array used when AM = 1.
        tremoloTable = [
            OPL3JSUtil.initArray(tremoloTableLength, 0),
            OPL3JSUtil.initArray(tremoloTableLength, 0)
        ];

        // This is undocumented. The tremolo starts at the maximum attenuation,
        // instead of at 0 dB:
        tremoloTable[0][0] = tremoloDepth[0];
        tremoloTable[1][0] = tremoloDepth[1];
        var counter = 0;
        // The first half of the triangle waveform:
        while (tremoloTable[0][counter] < 0) {
            counter++;
            tremoloTable[0][counter] = tremoloTable[0][counter - 1] + tremoloIncrement[0];
            tremoloTable[1][counter] = tremoloTable[1][counter - 1] + tremoloIncrement[1];
        }
        // The second half of the triangle waveform:
        while (tremoloTable[0][counter] > tremoloDepth[0] && counter < tremoloTableLength - 1) {
            counter++;
            tremoloTable[0][counter] = tremoloTable[0][counter - 1] - tremoloIncrement[0];
            tremoloTable[1][counter] = tremoloTable[1][counter - 1] - tremoloIncrement[1];
        }

        return tremoloTable;
    })();

    return OPL3Data;
})();
