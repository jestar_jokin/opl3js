//import "OPL3JSUtil.js"

var OperatorData = (function() {

    function OperatorData() {
    }

    OperatorData.AM1_VIB1_EGT1_KSR1_MULT4_Offset = 0x20; // int
    OperatorData.KSL2_TL6_Offset = 0x40; // int
    OperatorData.AR4_DR4_Offset = 0x60; // int
    OperatorData.SL4_RR4_Offset = 0x80; // int
    OperatorData._5_WS3_Offset = 0xE0; // int

    OperatorData.type = {
        NO_MODULATION : 'NO_MODULATION',
        CARRIER : 'CARRIER',
        FEEDBACK : 'FEEDBACK'
    }; // doesn't seem to be used.

    OperatorData.waveLength = 1024;
    OperatorData.multTable = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 10.0, 12.0, 12.0, 15.0, 15.0];
    OperatorData.ksl3dBtable = [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, -3, -6, -9],
        [0, 0, 0, 0, -3, -6, -9, -12],
        [0, 0, 0, -1.875, -4.875, -7.875, -10.875, -13.875],

        [0, 0, 0, -3, -6, -9, -12, -15],
        [0, 0, -1.125, -4.125, -7.125, -10.125, -13.125, -16.125],
        [0, 0, -1.875, -4.875, -7.875, -10.875, -13.875, -16.875],
        [0, 0, -2.625, -5.625, -8.625, -11.625, -14.625, -17.625],

        [0, 0, -3, -6, -9, -12, -15, -18],
        [0, -0.750, -3.750, -6.750, -9.750, -12.750, -15.750, -18.750],
        [0, -1.125, -4.125, -7.125, -10.125, -13.125, -16.125, -19.125],
        [0, -1.500, -4.500, -7.500, -10.500, -13.500, -16.500, -19.500],

        [0, -1.875, -4.875, -7.875, -10.875, -13.875, -16.875, -19.875],
        [0, -2.250, -5.250, -8.250, -11.250, -14.250, -17.250, -20.250],
        [0, -2.625, -5.625, -8.625, -11.625, -14.625, -17.625, -20.625],
        [0, -3, -6, -9, -12, -15, -18, -21]
    ];

    OperatorData.waveforms = function() { // replaces "loadWaveforms" static method
        var i;
        var waveforms = OPL3JSUtil.initArray(8, null); // double[]
        for (i = 0; i < 8; i++) {
            waveforms[i] = OPL3JSUtil.initArray(OperatorData.waveLength, 0);
        }

        // 1st waveform: sinusoid.
        var theta = 0;
        var thetaIncrement = 2.0 * Math.PI / 1024.0; // double

        // TODO: replace usage of 1024, 768, 512, 256 etc with divisions of OperatorData.waveLength.
        for (i = 0, theta = 0; i < 1024; i++, theta += thetaIncrement) {
            waveforms[0][i] = Math.sin(theta);
        }

        var sineTable = waveforms[0]; // double[]
        // 2nd: first half of a sinusoid.
        for (i = 0; i < 512; i++) {
            waveforms[1][i] = sineTable[i];
            waveforms[1][512 + i] = 0;
        }
        // 3rd: double positive sinusoid.
        for (i = 0; i < 512; i++)
            waveforms[2][i] = waveforms[2][512 + i] = sineTable[i];
        // 4th: first and third quarter of double positive sinusoid.
        for (i = 0; i < 256; i++) {
            waveforms[3][i] = waveforms[3][512 + i] = sineTable[i];
            waveforms[3][256 + i] = waveforms[3][768 + i] = 0;
        }
        // 5th: first half with double frequency sinusoid.
        for (i = 0; i < 512; i++) {
            waveforms[4][i] = sineTable[i * 2];
            waveforms[4][512 + i] = 0;
        }
        // 6th: first half with double frequency positive sinusoid.
        for (i=0; i < 256; i++) {
            waveforms[5][i] = waveforms[5][256 + i] = sineTable[i * 2];
            waveforms[5][512 + i] = waveforms[5][768 + i] = 0;
        }
        // 7th: square wave
        for (i=0; i < 512; i++) {
            waveforms[6][i] = 1;
            waveforms[6][512 + i] = -1;
        }
        // 8th: exponential
        var x; // double
        var xIncrement = 1 * 16.0 / 256.0; // double
        for (i = 0, x = 0; i < 512; i++, x += xIncrement) {
            waveforms[7][i] = Math.pow(2, -x);
            waveforms[7][1023 - i] = -Math.pow(2, -(x + 1 / 16.0));
        }

        return waveforms;
    }();

    OperatorData.log2 = function(x) {
        return Math.log(x) / Math.log(2);
    };

    return OperatorData;
})();