//import "OPL3JSUtil.js"
//import "ChannelData.js"

var Channel;

Channel = (function() {
    function Channel(opl3Chip, baseAddress) {
        this.opl3Chip = opl3Chip;
        this.channelBaseAddress = baseAddress; // int
        this.feedback = OPL3JSUtil.initArray(2, 0.0); // double[]
        (this.fnuml = this.fnumh = this.kon = this.block =
            this.cha = this.chb = this.chc = this.chd = this.fb = this.cnt = 0);
        this.output = OPL3JSUtil.initArray(4, 0.0); // double[]
    }

    // Factor to convert between normalized amplitude to normalized
    // radians. The amplitude maximum is equivalent to 8*Pi radians.
    Channel.toPhase = 4;

    Channel.prototype.update_2_KON1_BLOCK3_FNUMH2 = function() {
        var _2_kon1_block3_fnumh2 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData._2_KON1_BLOCK3_FNUMH2_Offset];

        // Frequency Number (hi-register) and Block. These two registers, together with fnuml,
        // sets the Channel´s base frequency;
        this.block = (_2_kon1_block3_fnumh2 & 0x1C) >> 2;
        this.fnumh = _2_kon1_block3_fnumh2 & 0x03;
        this._updateOperators();

        // Key On. If changed, calls Channel.keyOn() / keyOff().
        var newKon   = (_2_kon1_block3_fnumh2 & 0x20) >> 5; // int
        if (newKon != this.kon) {
            if (newKon == 1) {
                this._keyOn();
            } else {
                this._keyOff();
            }
            this.kon = newKon;
        }
    };

    Channel.prototype.update_FNUML8 = function() {
        var fnuml8 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData.FNUML8_Offset];
        // Frequency Number, low register.
        this.fnuml = fnuml8 & 0xFF;
        this._updateOperators();
    };

    Channel.prototype.update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1 = function() {
        var chd1_chc1_chb1_cha1_fb3_cnt1 = this.opl3Chip.registers[this.channelBaseAddress + ChannelData.CHD1_CHC1_CHB1_CHA1_FB3_CNT1_Offset];
        this.chd = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x80) >> 7;
        this.chc = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x40) >> 6;
        this.chb = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x20) >> 5;
        this.cha = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x10) >> 4;
        this.fb = (chd1_chc1_chb1_cha1_fb3_cnt1 & 0x0E) >> 1;
        this.cnt = chd1_chc1_chb1_cha1_fb3_cnt1 & 0x01;
        this._updateOperators();
    };

    Channel.prototype.updateChannel = function() {
        this.update_2_KON1_BLOCK3_FNUMH2();
        this.update_FNUML8();
        this.update_CHD1_CHC1_CHB1_CHA1_FB3_CNT1();
    };

    Channel.prototype._getInFourChannels = function(channelOutput) {
        var output = this.output;
        OPL3JSUtil.fillArray(output, 0.0); // double[]

        if (this.opl3Chip._new === 0) {
            output[0] = output[1] = output[2] = output[3] = channelOutput;
        } else {
            output[0] = (this.cha == 1) ? channelOutput : 0;
            output[1] = (this.chb == 1) ? channelOutput : 0;
            output[2] = (this.chc == 1) ? channelOutput : 0;
            output[3] = (this.chd == 1) ? channelOutput : 0;
        }

        return output;
    };

    // Abstract methods
    Channel.prototype.getChannelOutput = function() {
        // Override this method and return double[]
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._keyOn = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._keyOff = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    Channel.prototype._updateOperators = function() {
        // Override this method and return void
        throw new OPL3JSException('Method not implemented');
    };

    return Channel;
})();