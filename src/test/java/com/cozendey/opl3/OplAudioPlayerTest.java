package com.cozendey.opl3;

import org.junit.Test;

public class OplAudioPlayerTest {
    @Test
    public void testOPL3SoundOutput() {
        short[][] inputData = {
                {0xC0, 0x4},
                {0x21, 0x2A},
                {0x32, 0x2D},
                {0x44, 0x4B},
                {0xF5, 0x8A},
                {0xA7, 0x2},
                {0xA8, 0xAE},
                {0xBD, 0x20},
                {0xC5, 0xE},
                {0xC7, 0x1},
                {0xC8, 0x1},
                {0xB5, 0x33},
                {0xB5, 0x13},
                {0x2A, 0x1},
                {0x4A, 0x15},
                {0x6A, 0x25},
                {0x8A, 0x2F},
                {0xEA, 0x0},
                {0x2D, 0x21},
                {0x6D, 0x65},
                {0x8D, 0x6C},
                {0xED, 0x0},
                {0xC5, 0xA},
                {0x4D, 0x80},
                {0xA5, 0x63},
                {0xB5, 0xA},
                {0xB5, 0x2A},
                {0x29, 0x13},
                {0x49, 0x97},
                {0x69, 0x9A},
                {0x89, 0x12},
                {0xE9, 0x2},
                {0x2C, 0x91},
                {0x6C, 0x9B},
                {0x8C, 0x11},
                {0xEC, 0x0},
                {0xC4, 0xE},
                {0x4C, 0x80},
                {0xA4, 0x63},
                {0xB4, 0xA},
                {0xB4, 0x2A},
                {0x28, 0x13},
                {0x48, 0x97},
                {0x68, 0x9A},
                {0x88, 0x12},
                {0xE8, 0x2},
                {0x2B, 0x91},
                {0x6B, 0x9B},
                {0x8B, 0x11},
                {0xEB, 0x0},
                {0xC3, 0xE},
                {0x4B, 0x80},
                {0xA3, 0x63},
                {0xB3, 0xE},
                {0xB3, 0x2E}
        };

        OplAudioPlayer player = new OplAudioPlayer();
        player.writeData(inputData);
        player.renderAudio(3);

    }
}
