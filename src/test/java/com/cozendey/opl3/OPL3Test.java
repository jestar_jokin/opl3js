
package com.cozendey.opl3;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

public final class OPL3Test {

    @Test
    public void testEnvelopeGeneratorSetActualAttackRateHandlesInfinity() {
        EnvelopeGenerator obj = new EnvelopeGenerator();
        int attackRate, ksr, keyScaleNumber;
        attackRate = 0;
        ksr = 0;
        keyScaleNumber = 0;
        obj.setActualAttackRate(attackRate, ksr, keyScaleNumber);
        assertEquals(0, obj.xMinimumInAttack, 3.3219280948873626);
    }

    @Test
    public void testEnvelopeGeneratorSetActualAttackRate2() {
        EnvelopeGenerator obj = new EnvelopeGenerator();
        int attackRate, ksr, keyScaleNumber;
        attackRate = 9;
        ksr = 1;
        keyScaleNumber = 4;
        obj.setActualAttackRate(attackRate, ksr, keyScaleNumber);
        //assertEquals(-67.16794115266069, obj.envelope, 0);
        assertEquals(-0.030873851772649265, obj.xAttackIncrement, 0);
        assertEquals(6.069700902653147, obj.xMinimumInAttack, 0);
    }

    /*@Test
    public void testEnvelopeGeneratorCalculateActualRate() {
        EnvelopeGenerator obj = new EnvelopeGenerator();
        int attackRate, ksr, keyScaleNumber;
        attackRate = 9;
        ksr = 1;
        keyScaleNumber = 4;
        int result = obj.calculateActualRate(attackRate, ksr, keyScaleNumber);
        assertEquals(40, result);
    }*/

    @Test
    public void testOPL3Write() {
        // A bit pointless, this one.
        OPL3 obj = new OPL3();
        for (int bank = 0; bank < 2; bank++) {
            for (int reg = 0; reg < 256; reg++) {
                obj.write(bank, reg, 0xFF);
            }
        }
    }

    @Test
    public void testOPL3Read() {
        OPL3 obj = new OPL3();
        short[] result;

        short[][] inputData = {
                {0xC0, 0x4},
                {0x21, 0x2A},
                {0x32, 0x2D},
                {0x44, 0x4B},
                {0xF5, 0x8A},
                {0xA7, 0x2},
                {0xA8, 0xAE},
                {0xBD, 0x20},
                {0xC5, 0xE},
                {0xC7, 0x1},
                {0xC8, 0x1},
                {0xB5, 0x33},
                {0xB5, 0x13},
                {0x2A, 0x1},
                {0x4A, 0x15},
                {0x6A, 0x25},
                {0x8A, 0x2F},
                {0xEA, 0x0},
                {0x2D, 0x21},
                {0x6D, 0x65},
                {0x8D, 0x6C},
                {0xED, 0x0},
                {0xC5, 0xA},
                {0x4D, 0x80},
                {0xA5, 0x63},
                {0xB5, 0xA},
                {0xB5, 0x2A},
                {0x29, 0x13},
                {0x49, 0x97},
                {0x69, 0x9A},
                {0x89, 0x12},
                {0xE9, 0x2},
                {0x2C, 0x91},
                {0x6C, 0x9B},
                {0x8C, 0x11},
                {0xEC, 0x0},
                {0xC4, 0xE},
                {0x4C, 0x80},
                {0xA4, 0x63},
                {0xB4, 0xA},
                {0xB4, 0x2A},
                {0x28, 0x13},
                {0x48, 0x97},
                {0x68, 0x9A},
                {0x88, 0x12},
                {0xE8, 0x2},
                {0x2B, 0x91},
                {0x6B, 0x9B},
                {0x8B, 0x11},
                {0xEB, 0x0},
                {0xC3, 0xE},
                {0x4B, 0x80},
                {0xA3, 0x63},
                {0xB3, 0xE},
                {0xB3, 0x2E}
        };
        short[] expected = {
                158,
                167,
                177,
                188,
                200,
                212,
                225,
                232,
                236,
                235,
                230,
                220,
                207,
                188,
                168,
                144,
                120,
                94,
                71,
                48,
                29,
                13,
                0,
                -9,
                -19,
                -25,
                -25,
                -26,
                -24,
                -19,
                -14,
                -2,
                8,
                23,
                39,
                59,
                79,
                101,
                129,
                159
        };
        for (short[] inputDatum : inputData) {
            obj.write(0, inputDatum[0], inputDatum[1]);
        }
        for (int run = 0; run < 64; run++) {
            result = obj.read();
        }
        short[] finalResult = new short[40];
        for (int outRun = 0; outRun < 40; outRun++) {
            result = obj.read();
            finalResult[outRun] = result[0];
        }
        /*for (short aShort : finalResult) {
            System.out.println(aShort);
        }*/
        assertArrayEquals(expected, finalResult);
    }

    @Test
    public void testPhaseGenerator() {
        // crap
        int dvb = OPL3.dvb;
        int vibratoIndex = OPL3.vibratoIndex;

        PhaseGenerator pg = new PhaseGenerator();
        pg.setFrequency(4, 5, 6);
        assertEquals(0.000732421875, pg.phaseIncrement, 0);
    }

    @Test
    public void testOperatorOutput() {
        double modulator = 7.981520769387E-9;
        double outputPhase = 0.00466156005859375;
        double[] waveform = OperatorData.waveforms[0];

        Operator op = new Operator(11);
        op.envelope = 1.5102608805728618E-7;
        double output = op.getOutput(modulator, outputPhase, waveform);
        assertEquals(3.7063657399353347E-9, output, 0);
    }

    @Test
    public void testOPL3DataCalculateIncrement() {
        double begin, end, period;
        begin = 3.3219280948873626;
        end = -1.127920161104575;
        period = 0.0029;
        double result = OPL3Data.calculateIncrement(begin, end, period);
        assertEquals(-0.030873851772649265, result, 0);
    }

}